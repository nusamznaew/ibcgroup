<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->string('place');
            $table->string('preview');
            $table->text('banner');
            $table->integer('asking_price');
            $table->integer('cash_flow');
            $table->integer('ebitda');
            $table->integer('ffe_price');
            $table->integer('gross_income');
            $table->integer('rent');
            $table->integer('inventory_price');
            $table->integer('founded');
            $table->text('description');
            $table->string('inventory');
            $table->string('ffe');
            $table->string('object');
            $table->string('support');
            $table->string('reason_for_selling');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
