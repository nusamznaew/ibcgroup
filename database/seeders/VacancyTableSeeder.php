<?php

namespace Database\Seeders;

use App\Models\Vacancy;
use Illuminate\Database\Seeder;

class VacancyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vacancy::create([
            'vacancy' => 'Copywriter',
            'description' => 'Мы ищем специалиста, который понимает геометрию букв и грамотно может написать историю жизни человека.',
            'detailed' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Non arcu risus quis varius. Est ante in nibh mauris cursus. Mattis enim ut tellus elementum sagittis vitae et leo. Elit at imperdiet dui accumsan sit. Pharetra magna ac placerat vestibulum lectus mauris ultrices eros. Vitae nunc sed velit dignissim. Laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean. Venenatis tellus in metus vulputate eu scelerisque felis.'
        ]);

        Vacancy::create([
            'vacancy' => 'Tik-Tok Creator',
            'description' => 'Мы в поисках начинающего специалиста, который хочет развиваться в самой креативной социальной сети, который не боится предлагать идеи и защищать их перед клиентом.',
            'detailed' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Non arcu risus quis varius. Est ante in nibh mauris cursus. Mattis enim ut tellus elementum sagittis vitae et leo. Elit at imperdiet dui accumsan sit. Pharetra magna ac placerat vestibulum lectus mauris ultrices eros. Vitae nunc sed velit dignissim. Laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean. Venenatis tellus in metus vulputate eu scelerisque felis.'
        ]);

        Vacancy::create([
            'vacancy' => 'SMM специалист',
            'description' => 'Мы ищем специалиста, который понимает геометрию букв и грамотно может написать историю жизни человека.',
            'detailed' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Non arcu risus quis varius. Est ante in nibh mauris cursus. Mattis enim ut tellus elementum sagittis vitae et leo. Elit at imperdiet dui accumsan sit. Pharetra magna ac placerat vestibulum lectus mauris ultrices eros. Vitae nunc sed velit dignissim. Laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean. Venenatis tellus in metus vulputate eu scelerisque felis.'
        ]);
    }
}
