<?php

namespace Database\Seeders;

use App\Models\Advice;
use Illuminate\Database\Seeder;

class AdviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Advice::create([
            'category' => 'ОБРАЗОВАНИЕ В США',
            'title' => 'Альберт Сагитин, 28 лет, как получить образвание в США бесплатно',
            'slug' => 'albert-sagitin',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit pellentesque habitant morbi tristique senectus et. Tincidunt tortor aliquam nulla facilisi cras. Sed id semper risus in hendrerit gravida. Purus gravida quis blandit turpis cursus in hac habitasse.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Non arcu risus quis varius. Est ante in nibh mauris cursus. Mattis enim ut tellus elementum sagittis vitae et leo. Elit at imperdiet dui accumsan sit. Pharetra magna ac placerat vestibulum lectus mauris ultrices eros. Vitae nunc sed velit dignissim. Laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean. Venenatis tellus in metus vulputate eu scelerisque felis.'
        ]);

        Advice::create([
            'category' => 'ОБРАЗОВАНИЕ В КАНАДЕ',
            'title' => 'Цены на продукты, бензин, жилье, одежду в США в 2021 году',
            'slug' => 'tseny-na-produkty',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis vitae et leo duis ut diam. Consectetur adipiscing elit pellentesque habitant morbi tristique senectus et. Fames ac turpis egestas integer eget aliquet. Arcu bibendum at varius vel pharetra vel turpis nunc.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisl pretium fusce id velit ut tortor pretium. At ultrices mi tempus imperdiet nulla malesuada pellentesque. Accumsan tortor posuere ac ut. Urna et pharetra pharetra massa massa. Non arcu risus quis varius quam quisque. Tincidunt praesent semper feugiat nibh sed. At varius vel pharetra vel turpis. Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. At erat pellentesque adipiscing commodo elit.'
        ]);

        Advice::create([
            'category' => 'СОЦИАЛЬНАЯ АДАПТАЦИЯ',
            'title' => 'Mauris augue neque gravida in fermentum et sollicitudin ac orci',
            'slug' => 'mauris-augue',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dui vivamus arcu felis bibendum. Sit amet nisl purus in mollis. Nunc id cursus metus aliquam. Arcu dictum varius duis at consectetur lorem donec massa sapien.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut lectus arcu bibendum at varius vel pharetra vel. Nunc vel risus commodo viverra maecenas. Augue interdum velit euismod in. Pretium aenean pharetra magna ac placerat. Amet cursus sit amet dictum sit amet justo donec enim. Quis blandit turpis cursus in hac habitasse. Vestibulum rhoncus est pellentesque elit ullamcorper. Pharetra convallis posuere morbi leo. Non sodales neque sodales ut etiam sit amet.'
        ]);

        Advice::create([
            'category' => 'ОБРАЗОВАНИЕ В США',
            'title' => 'Elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi',
            'slug' => 'elit-duis',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fermentum et sollicitudin ac orci. Sagittis eu volutpat odio facilisis mauris sit. Lorem donec massa sapien faucibus et. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu non sodales neque sodales ut etiam sit amet. Amet risus nullam eget felis eget. Cras ornare arcu dui vivamus arcu felis bibendum ut tristique. Viverra suspendisse potenti nullam ac. Amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan. Gravida dictum fusce ut placerat. Malesuada pellentesque elit eget gravida cum sociis natoque. Donec ac odio tempor orci dapibus ultrices. Magna eget est lorem ipsum dolor sit amet consectetur adipiscing.'
        ]);

        Advice::create([
            'category' => 'ОБРАЗОВАНИЕ В КАНАДЕ',
            'title' => 'Blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque',
            'slug' => 'blandit-volutpat',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas sed sed risus pretium quam vulputate. Aliquam purus sit amet luctus venenatis lectus. Ullamcorper a lacus vestibulum sed arcu non odio. Sem nulla pharetra diam sit amet nisl suscipit adipiscing.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultricies mi eget mauris pharetra et ultrices neque. Aenean vel elit scelerisque mauris. Hac habitasse platea dictumst quisque sagittis purus sit. Nullam non nisi est sit amet facilisis magna. Mauris sit amet massa vitae tortor condimentum lacinia quis vel. Ac feugiat sed lectus vestibulum. Mauris pellentesque pulvinar pellentesque habitant morbi. Pellentesque diam volutpat commodo sed egestas egestas. Non odio euismod lacinia at.'
        ]);

        Advice::create([
            'category' => 'СОЦИАЛЬНАЯ АДАПТАЦИЯ',
            'title' => 'In egestas erat imperdiet sed euismod nisi porta lorem mollis',
            'slug' => 'in-egestas',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet cursus sit amet dictum sit amet justo. Viverra suspendisse potenti nullam ac tortor vitae purus. Adipiscing tristique risus nec feugiat in fermentum. Amet justo donec enim diam vulputate ut pharetra sit.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla malesuada pellentesque elit eget gravida cum. Arcu odio ut sem nulla pharetra. In iaculis nunc sed augue lacus viverra vitae congue eu. Sed libero enim sed faucibus turpis in. Commodo nulla facilisi nullam vehicula ipsum. Facilisis mauris sit amet massa vitae tortor condimentum. Varius morbi enim nunc faucibus a pellentesque. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Vestibulum morbi blandit cursus risus at ultrices mi.'
        ]);

        Advice::create([
            'category' => 'ОБРАЗОВАНИЕ В США',
            'title' => 'In pellentesque massa placerat duis ultricies lacus sed turpis tincidunt',
            'slug' => 'in-pellentesque',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Purus gravida quis blandit turpis cursus in hac. Viverra maecenas accumsan lacus vel facilisis volutpat. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus. Pharetra sit amet aliquam id diam maecenas.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dictum non consectetur a erat nam at lectus. Leo in vitae turpis massa sed elementum tempus. Et netus et malesuada fames ac turpis egestas sed. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Nunc sed augue lacus viverra vitae congue eu consequat ac. Arcu non odio euismod lacinia at quis. Quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna. Nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit amet. Ut sem viverra aliquet eget sit amet tellus.'
        ]);

        Advice::create([
            'category' => 'ОБРАЗОВАНИЕ В КАНАДЕ',
            'title' => 'Lacus vestibulum sed arcu non odio euismod lacinia at quis',
            'slug' => 'lacus-vestibulum',
//            'details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lacus vel facilisis volutpat est. Sit amet nisl purus in mollis nunc sed. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Feugiat pretium nibh ipsum consequat nisl vel.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id cursus metus aliquam eleifend mi in nulla posuere. Cursus sit amet dictum sit amet justo donec enim. Lacinia at quis risus sed. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Turpis nunc eget lorem dolor. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit. Aliquet enim tortor at auctor. Ornare suspendisse sed nisi lacus sed viverra tellus in. Volutpat consequat mauris nunc congue.'
        ]);
    }
}
