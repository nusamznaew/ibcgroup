<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HeadlinesTableSeeder::class);
        $this->call(AdviceTableSeeder::class);
        $this->call(InstancesTableSeeder::class);
        $this->call(FeedbackTableSeeder::class);
        $this->call(VacancyTableSeeder::class);
    }
}
