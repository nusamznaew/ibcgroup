(function () {
    var client = algoliasearch('GVRNJHSVE6', '4aa2f0a3f1cf0d749d5793208cf43feb');
    var index = client.initIndex('headlines');
    //initialize autocomplete on search input (ID selector must match)
    autocomplete('#aa-search-input',
        {hint: false}, {
            source: autocomplete.sources.hits(index),
            //value to be displayed in input control after user's suggestion selection
            displayKey: 'title',
            //hash of templates used when rendering dataset
            templates: {
                suggestion: function (suggestion) {
                    return `
                        <div class="algolia-result">
                            <span>
                                ${suggestion._highlightResult.title.value.replace(/^(.{50}[^\s]*).*/, "$1")}
                            </span>
                            <span>${suggestion._highlightResult.category.value}</span>
                        </div>
                    `;
                }
            }
        }).on('autocomplete:selected', function (event, suggestion, dataset) {
            // console.log(suggestion)
        window.location.href = window.location.origin + '/headlines/' + suggestion.slug;
    });
})();

