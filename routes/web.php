<?php

use App\Http\Controllers\AdviceController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\HeadlineController;
use App\Http\Controllers\InstanceController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\MainPageController;
use App\Http\Controllers\MasterClassController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\VacancyController;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin Page
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '[a-zA-Z]{2}'],
    'middleware' => 'setLocale'
], function () {
    //Dynamic Pages
    Route::get('/', [MainPageController::class, 'index'])->name('main');

    Route::get('/search', [MainPageController::class, 'search'])->name('search');

    Route::get('/headlines', [HeadlineController::class, 'index'])->name('headlines.index');
    Route::get('/headlines/{headline}', [HeadlineController::class, 'show'])->name('headlines.show');

    Route::get('/advices', [AdviceController::class, 'index'])->name('advices.index');
    Route::get('/advices/{advice}', [AdviceController::class, 'show'])->name('advices.show');

    Route::get('/instances', [InstanceController::class, 'index'])->name('instances.index');
    Route::get('/instances/{slug}', [InstanceController::class, 'show'])->name('instances.show');

    Route::get('/feedbacks', [FeedbackController::class, 'index'])->name('feedbacks.index');
    Route::get('/feedbacks/{feedback}', [FeedbackController::class, 'show'])->name('feedbacks.show');

    Route::get('/masterclasses', [MasterClassController::class, 'index'])->name('masterclasses.index');
    Route::get('/masterclasses/{masterclass}', [MasterClassController::class, 'show'])->name('masterclasses.show');
    Route::post('/masterclasses/send', [MasterClassController::class, 'send'])->name('masterclasses.send');

    Route::get('/businesses', [BusinessController::class, 'index'])->name('businesses.index');
    Route::get('/businesses/{business}', [BusinessController::class, 'show'])->name('businesses.show');
    Route::post('/businesses/send', [BusinessController::class, 'send'])->name('businesses.send');


    //Static Pages
    Route::get('/contacts', [ContactController::class, 'index'])->name('contacts.index');


    Route::get('/ibc-group-is', function () {
        return view('pages.ibc-group-is');
    })->name('ibc-group-is');


    Route::get('/services', [ServiceController::class, 'index'])->name('service.index');


    Route::get('/vacancy', [VacancyController::class, 'index'])->name('vacancy.index');

    Route::post('/vacancy/send', [VacancyController::class, 'send'])->name('vacancy.send');


    Route::get('/mass-media', [MediaController::class, 'index'])->name('mass-media');

    Route::get('/visa-eb1a', function () {
        return view('pages.visa-eb1a');
    })->name('visa-eb1a');


    Route::get('/success', function () {
        return view('pages.success');
    })->name('success');


    //Form
    Route::post('/send/form', [MailController::class, 'send'])->name('send-form');


    //Clear Cache
    Route::get('/clear_cache', function () {

        \Artisan::call('optimize');

        dd("Cache is cleared");
    });
});
