<div class="header d-flex justify-content-lg-between justify-content-between align-items-center">
    <form class="d-lg-block d-none" action="{{ route('search', app()->getLocale()) }}" method="GET">
        @csrf
        <div class="d-lg-flex d-none search-bar">
            <input type="text" name="query" value="" class="form-control search-input" placeholder="" aria-label="Recipient's username"
                   aria-describedby="button-addon2">
            <button class="btn" value="Search" type="submit" id="button-addon2">{{__('main.search')}}</button>
        </div>
    </form>
    @include('partials.burger')
    <a href="#" class="btn call-back d-flex justify-content-center align-items-center" data-bs-toggle="modal"
       data-bs-target="#requestModal">{{__('main.callback')}}</a>
</div>
