<div class="navbar-block d-lg-flex d-none flex-column">
    <div class="d-flex logo">
        <a href="{{ route('main', app()->getLocale())}}"><img src="{{ asset('img/logo.png') }}" width="124" alt="logo"></a>
    </div>
    <div class="d-flex flex-column links">
        <div class="d-flex flex-column links">
            <a href="{{ route('main', app()->getLocale()) }}"><b>{{__('main.home')}}</b></a>
            <a href="{{ route('ibc-group-is', app()->getLocale()) }}"><b>{{__('main.what_is_ibc')}}</b></a>
            <a href="{{ route('service.index', app()->getLocale()) }}"><b>{{__('main.our_services')}}</b></a>
            <a href="{{ route('vacancy.index', app()->getLocale()) }}"><b>{{__('main.vacancies')}}</b></a>
            <a href="{{ route('contacts.index', app()->getLocale()) }}"><b>{{__('main.contacts')}}</b></a>

            <div class="line"></div>
            <a href="{{ route('headlines.index', app()->getLocale()) }}">{{__('main.news')}}</a>

            <a href="{{ route('advices.index', app()->getLocale()) }}">{{__('main.useful')}}</a>

            <a href="{{ route('masterclasses.index', app()->getLocale()) }}">{{__('main.master_classes')}}</a>

            <a href="{{ route('instances.index', app()->getLocale()) }}">{{__('main.cases')}}</a>

            <a href="{{ route('feedbacks.index', app()->getLocale()) }}">{{__('main.reviews')}}</a>

            <div class="line"></div>
            <a href="https://ibcg-usa.kz/" target="_blank" rel="noopener noreferrer">{{__('main.work_and_study_usa')}}</a>
            <a href="https://one-ibc.kz/" target="_blank" rel="noopener noreferrer">{{__('main.preparing_to_enter_the_us')}}</a>
            <a href="https://usanaruss.com/" target="_blank" rel="noopener noreferrer">{{__('main.social_adaptation_in_usa')}}</a>
            <a href="https://kids.ibcg2.kz/" target="_blank" rel="noopener noreferrer">{{__('main.language_camp_usa')}}</a>
            <a href="https://trip.usanaruss.com/" target="_blank" rel="noopener noreferrer">{{__('main.business_tours_usa')}}</a>
            <a href="https://biz.usanaruss.com/" target="_blank" rel="noopener noreferrer">{{__('main.starting_a_business_in_usa')}}</a>
            <a href="{{ route('businesses.index', app()->getLocale()) }}" target="_blank" rel="noopener noreferrer">{{__('main.buying_business_in_usa')}}</a>
            <a href="https://turkey.ibcgwork.com/" target="_blank" rel="noopener noreferrer">{{__('main.working_in_turkey')}}</a>
        </div>
    </div>
</div>
