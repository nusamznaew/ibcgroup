<div class="burger d-lg-none d-flex align-items-center">
    <div class="wrapper">
        <!-- Sidebar -->
        <nav id="sidebar">

            <form action="{{ route('search', app()->getLocale()) }}" method="GET">
                @csrf
                <div class="sidebar-header">
                    <input type="text" name="query" value="" placeholder="🔍 &nbsp; &nbsp;Найдите" aria-label="default" aria-describedby="button-addon2">
                </div>
            </form>

            <div class="d-flex flex-column links">
                {{-- {{  menu('Главная', 'partials.menus.main') }}
                <div class="line"></div>
                {{ menu('Меню-2', 'partials.menus.second-menu') }}
                <div class="line"></div>
                {{ menu('Меню-3', 'partials.menus.third-menu') }} --}}
            </div>
        </nav>

        <a href="#" id="sidebarCollapse">
            <img src="/img/burger.png" alt="" width="27" height="18">
            Меню
        </a>

        <!-- Dark Overlay element -->
        <div class="overlay"></div>
    </div>
</div>
