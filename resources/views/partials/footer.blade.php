<footer>
    <div class="container">
        <div class="d-flex justify-content-between m-block">
            <div class="empty-block d-lg-flex d-none flex-column">
            </div>
            <div class="cont-block d-flex flex-column">
                <div class="top d-flex flex-lg-row flex-wrap justify-content-lg-between justify-content-start">
                    <a href="{{ route('headlines.index', app()->getLocale()) }}">{{ __('main.news') }}</a>
                    <a href="{{ route('advices.index', app()->getLocale()) }}">{{__('main.useful')}}</a>
                    <a href="{{ route('service.index', app()->getLocale()) }}">{{__('main.our_services')}}</a>
                    <a href="{{ route('mass-media', app()->getLocale()) }}">{{__('main.media_about_us')}}</a>
                    <a href="{{ route('instances.index', app()->getLocale()) }}">{{__('main.cases')}}</a>
                    <a href="{{ route('contacts.index', app()->getLocale()) }}">{{__('main.contacts')}}</a>
                    <a href="">{{__('main.quality_department_support_ibcg_kz')}}</a>
                </div>
                <div class="bottom d-flex flex-lg-row flex-column">
                    <div class="left">
                        <p>
                            {!! __('main.footer_left') !!}
                        </p>
                    </div>
                    <div class="right d-flex flex-lg-row flex-column">
                        <div class="d-flex flex-column">
                            <p>{!! __('main.footer_center') !!}</p>
                            <div class="d-flex social">
                                <a href="https://www.instagram.com/ibcgroup.kz/" target="_blank"
                                   rel="noopener noreferrer">
                                    <i class="bi bi-instagram"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UCdxuqrduYCTScxXUfsVxbMQ" target="_blank"
                                   rel="noopener noreferrer">
                                    <i class="bi bi-youtube"></i>
                                </a>
                            </div>
                        </div>
                        <div class="d-flex flex-column ms-lg-auto ms-0 mt-lg-0 mt-3">
                            <a href="tel:+77071867777">+7 (707) 186-77-77</a>
                            <a href="tel:222">{!! __('main.internal_number') !!}</a>
                            <a href="" class="btn btn-callback" data-bs-toggle="modal" data-bs-target="#requestModal">{!! __('main.callback') !!}</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center align-items-center agency">
                    {!! __('main.developed') !!}
                    &nbsp;
                    <a href="https://ibcagency.kz/" target="_blank" rel="noopener noreferrer"><img class="agency-logo"
                                                                                                   src="{{ asset('img/agency-logo.png') }}"
                                                                                                   alt=""
                                                                                                   loading="lazy"></a>
                </div>
            </div>
        </div>
    </div>

</footer>
