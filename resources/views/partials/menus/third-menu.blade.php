@foreach($items as $menu_item)
    @if($menu_item->link() === 'https://ibcg-usa.kz/')
        <a href="{{ $menu_item->link() }}" target="_blank" rel="noopener noreferrer">{{ $menu_item->title }}</a>
    @else
        <a href="{{ $menu_item->link() }}" target="_blank" rel="noopener noreferrer">{{ $menu_item->title }}</a>
    @endif
@endforeach
