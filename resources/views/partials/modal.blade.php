<!-- Modal -->
<div class="modal fade" id="requestModal" tabindex="-1" aria-labelledby="requestModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modalka d-flex flex-column position-relative">
                <button type="button" class="btn-close position-absolute" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="big-txt">Оставить заявку на консультацию</div>
                <div class="small-txt">Заполни поля ниже
                    и мы свяжемся с тобой
                    для консультации</div>
                <form action="{{ route('send-form', app()->getLocale()) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" name="name" placeholder="Имя" aria-label="default" required>
					<select name="city" class="form-select" placeholder="Город" aria-label="default" required>
  						<option value="Алматы">Алматы</option>
						<option value="Актобе">Актобе</option>
						<option value="Актау">Актау</option>
						<option value="Арал">Арал</option>
						<option value="Атырау">Атырау</option>
						<option value="Байконур">Байконур</option>
						<option value="Жанаозен">Жанаозен</option>
						<option value="Жезказган">Жезказган</option>
						<option value="Караганды">Караганды</option>
						<option value="Кокшетау">Кокшетау</option>
						<option value="Костанай">Костанай</option>
						<option value="Кулсары">Кулсары</option>
						<option value="Кызылорда">Кызылорда</option>
						<option value="Нур-Султан">Нур-Султан</option>
						<option value="Павлодар">Павлодар</option>
						<option value="Петропавловск">Петропавловск</option>
						<option value="Семей">Семей</option>
						<option value="Талдыкорган">Талдыкорган</option>
						<option value="Тараз">Тараз</option>
						<option value="Туркестан">Туркестан</option>
						<option value="Уральск">Уральск</option>
						<option value="Усть-Каменогорск">Усть-Каменогорск</option>
						<option value="Шымкент">Шымкент</option>
					</select>
                    <input type="tel" name="phone" class="phone" placeholder="Телефон" aria-label="default" required>
					<select name="point" class="form-select" placeholder="А откуда Вы узнали о нашей компании?" aria-label="default" required>
						<option value="" disabled selected hidden>А откуда Вы узнали о нашей компании?</option>
  						<option value="YouTube">YouTube</option>
						<option value="Instagram">Instagram</option>
						<option value="Таргетированная реклама">Таргетированная реклама</option>
					</select>
                    <button type="submit" class="submit submit-button">Оставить заявку</button>
                </form>
            </div>
        </div>
    </div>
</div>
