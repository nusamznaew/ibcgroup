@extends('layouts.layout')

@section('title', $masterclass->title)

@section('content')
    <div class="masterclass-page d-flex flex-lg-row flex-column justify-content-between">
        <div class="left">
            <div class="breadcrumbs">
                <a class="link" href="{{ route('masterclasses.index') }}">Мастер классы</a>
                <p class="d-inline date">{{ $masterclass->created_at->format('d M Y') }}</p>
            </div>
            <h2>{{ $masterclass->title }}</h2>
            <div class="d-flex flex-column">
                <img class="image" src="/storage/{{ $masterclass->image }}" alt="">
                <div class="d-flex flex-column details">
					@if(!empty($masterclass->date))
						<div><i class="bi bi-calendar-event"></i> {{ $masterclass->date->translatedFormat('jS F') }} в {{ $masterclass->date->format('H:i') }}</div>
					@endif
                    <div><i class="bi bi-geo-alt"></i> {{ $masterclass->place }}</div>
					@if(!empty($masterclass->price))
                    	<div><i class="bi bi-cash"></i>{{ $masterclass->price }} тг</div>
					@endif
                </div>
            </div>
            <div class="d-flex flex-column">
                <div class="description">
                    {!! $masterclass->description !!}
                </div>
            </div>
            <h3>Оставить заявку</h3>
            <form action="{{ route('masterclasses.send') }}" method="post" class="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="mb-3">
                    <label for="master-name" class="form-label">Имя</label>
                    <input name="name" type="text" class="form-control" id="master-name" placeholder="Имя" required>
                </div>
                <div class="mb-3">
                    <label for="master-email" class="form-label">E-mail</label>
                    <input name="email" type="email" class="form-control" id="master-email" placeholder="E-mail" required>
                </div>
                <div class="mb-3">
                    <label for="master-phone" class="form-label">Телефон</label>
                    <input name="phone" type="tel" class="form-control phone" id="master-phone" placeholder="Телефон" required>
                </div>
                <input name="masterclass" type="text" hidden value="{{ $masterclass->title }}">
                <div class="mb-3">
                    <button class="btn submit-button" type="submit">Оставить заявку</button>
                </div>
            </form>
            <div id="disqus_thread"></div>
        </div>
        <div class="right similar">
            @foreach($similarMasterclasses as $masterclass)
                <div class="box d-flex flex-row">
                    <a href="{{ route('masterclasses.show', $masterclass->slug) }}">
                        <div class="img" style="background-image: url('{{ asset('storage/'. $masterclass->image) }}')"></div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://ibcportal-kz-1.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection
