@extends('layouts.layout')

@section('title', 'Полезное')

@section('description', 'Статьи на темы: жизнь в США, питание и цены в Америке, лайфхаки по изучению английского языка.')

@section('keywords', 'питаниевсша, ценывсша, лайфхакипоизучениюанглийскогоязыка, какполучитьстуденческуювизу, визаf1, полезноесша, сколькостоитжизньвамерике, статьиобамерике, визаf2, какуехатьвсша')

@section('content')
    <div class="d-flex flex-wrap advices-page">
        @foreach($advices as $advice)
            <div class="d-flex flex-column box">
                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="img">
					<div class="picha" style="background-image: url('{{ asset('storage/'. $advice->image) }}')"></div>
				</a>
                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="edu">{{ $advice->category }}</a>
                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="big-text">{{ Str::limit($advice->title, 60) }}</a>
                <div class="d-flex small-text">
                    <p>{{ $advice->created_at->format('d M Y') }}</p>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('extra-js')

@endsection
