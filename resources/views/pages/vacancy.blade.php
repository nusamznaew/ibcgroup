@extends('layouts.layout')

@section('title', 'Вакансии')

@section('content')
    <div class="page-vacancy">
        <div class="d-flex flex-lg-row flex-column headcrab align-items-center justify-content-between">
            <div class="d-flex big-txt align-items-center">
                Вакансии
                <a href="https://www.instagram.com/ibcgroup.team/">
                    <img src="{{ asset('img/instagram2.svg') }}" alt="">
                </a>
            </div>
            <div class="small-txt">
                Люди - главная ценность компании. Мы верим в каждого. У нас люди находят себя и свое призвание. А мы
                вдохновляем новыми горизонтами и поддерживаем во всех начинаниях.
            </div>
        </div>
        <div class="vacancy">
            <div class="container">
                <div class="header d-flex align-items-center">
                    <p class="header">Мы ищем:</p>
                </div>
            </div>
            <div class="vacancies">
                <div class="accordion" id="accordionVacancy">
                    @foreach($vacancies as $vacancy)
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading{{ $loop->index }}">
                                <a class="accordion-button collapsed" type="button"
                                   data-bs-toggle="collapse"
                                   data-bs-target="#collapse{{ $loop->index }}" aria-expanded="true" aria-controls="collapse{{ $loop->index }}">
                                    <div class="container">
                                        <div
                                            class="d-flex flex-lg-row flex-column justify-content-between align-items-lg-center align-items-start content">
                                            <h3>{{ $vacancy->vacancy }}</h3>
                                            <div class="d-flex right align-items-center justify-content-between">
                                                <p class="description">{{ $vacancy->description }}</p>
                                                <svg version="1.1" width="50" height="50" viewBox="0 0 80 80"
                                                     fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="40" cy="40" r="39.5"/>
                                                    <path
                                                        d="M37.992 42.264H29.016V38.904H37.992V29.208H41.496V38.904H50.472V42.264H41.496V51.96H37.992V42.264Z"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </h2>
                            <div id="collapse{{ $loop->index }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $loop->index }}"
                                 data-bs-parent="#accordionVacancy">
                                <div class="accordion-body container">
                                    <div class="d-flex flex-lg-row flex-column detailed">
                                        <p class="big-txt">
                                            Что нужно делать
                                        </p>
                                        {!! $vacancy->detailed !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="container">
                <div class="form-vacancy d-flex flex-lg-row flex-column">
                    <div class="left">
                        <p class="big-txt">Откликнуться <br>
                            на вакансию</p>
                        <p class="small-txt">Вашей вакансии нет в списке?<br>
                            Напишите нам если хотите в команду,<br>
                            возможно для вас найдется вакантное место</p>
                    </div>
                    <div class="right">
                        <form class="ajax-form" id="vacancy_form" action="{{ route('vacancy.send', [app()->getLocale()]) }}" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="blank">
                                <label for="name" class="form-label">Имя</label>
                                <input type="text" name="name" id="name" class="form-control" required>

                                <label for="position" class="form-label">Должность</label>
                                <input type="text" name="position" id="position" class="form-control" required>

                                <label for="phone" class="form-label phone">Контакт для связи</label>
                                <input type="tel" name="phone" id="phone" class="form-control phone" required>

                                <label for="name" class="form-label">Ссылка на соцсети</label>
                                <input type="text" name="social" id="name" class="form-control" required>

                                <label for="description" class="form-label">Расскажите о себе и своем опыте
                                    работы</label>
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Leave a comment here" name="description"
                                              id="description" aria-label="default" required></textarea>
                                </div>
                            </div>
                            <div class="resume">
                                <label for="formFile" class="form-label">Резюме</label>
                                <input class="form-control" name="file" type="file" id="formFile" required>
                            </div>
                            <button type="submit" value="Submit" class="submit-button">
                                Отправить отклик
                            </button>
                            <p class="agreement">Нажимая кнопку отправить, вы соглашаетесь <br>
                                на обработку персональных данных.</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
@endsection
