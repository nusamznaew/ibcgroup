@extends('layouts.layout')

@section('title', 'Контакты')

@section('description', '+7 (707) 186-77-77, +7 (708) 800-24-14
Головной офис в Алматы мкр. Коктем 2, д. 22, 4 этаж')

@section('keywords', 'образованиевсша, ibcg, ibcgroup, образованиезарубежом, жизньвамерике, жизньвсша, контактыibcgroup, контактыibcg')

@section('content')
    <div class="d-flex flex-column contacts-page">
        <h2>{!! setting('kontakty.title') !!}</h2>
        <div class="d-lg-flex d-none flex-column justify-content-between post-mark position-absolute">
            <div class="content-block">
                {!! setting('kontakty.post-mark') !!}
            </div>
            <img src="img/line.png" alt="">
        </div>
        <div class="d-flex flex-column box-1">
            <p class="big-txt">Головной офис:</p>
            <p class="small-txt">г. Алматы мкр. Коктем 2, д.22. 4 этаж</p>
            <a href="" class="map-link"><i class="bi bi-geo-alt"></i> Показать на карте</a>
            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ac438d248935b0e43ede29bdb0c8a0b7fbd6a2dc358b42105422aecd74f340d6e&amp;source=constructor" class="map" frameborder="0"></iframe>
            <p class="number">+7 (707) 186-77-77</p>
        </div>
        <div class="d-flex flex-column box-2">
            {!! setting('kontakty.sub-office') !!}
        </div>
        <div class="d-lg-none d-flex flex-column post-mark">
            <div class="content-block">
                {!! setting('kontakty.post-mark') !!}
            </div>
            <img src="img/line.png" alt="">
        </div>
    </div>
@endsection

@section('extra-js')

@endsection
