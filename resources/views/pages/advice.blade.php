@extends('layouts.layout')

@section('title', $advice->title)

@section('content')
    <div class="advice-page d-flex flex-lg-row flex-column justify-content-between">
        <div class="left">
            <div class="breadcrumbs">
                <a class="link" href="{{ route('headlines.index') }}">Полезное</a>
                <p class="d-inline date">{{ $advice->created_at->format('d M Y') }}</p>
            </div>
            <h2>{{ $advice->title }}</h2>
            <div class="d-flex flex-column">
                <div class="description">
                    {!! $advice->description !!}
                </div>
            </div>
            <div id="disqus_thread"></div>
        </div>
        <div class="right similar">
            @foreach($similarAdvices as $advice)
                <div class="box d-flex flex-row">
                    <a href="{{ route('advices.show', $advice->slug) }}">
                        <div class="img" style="background-image: url('{{ asset('storage/'. $advice->image) }}')"></div>
                    </a>
                    <div class="d-flex flex-column">
                        <a class="title" href="{{ route('advices.show', $advice->slug) }}">{{ Str::limit($advice->title, 50) }}</a>
                        <div class="date">{!! $advice->created_at->format('d M Y') !!}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://ibcportal-kz-1.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection
