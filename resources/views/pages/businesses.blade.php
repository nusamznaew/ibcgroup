@extends('layouts.layout')

@section('title', 'Бизнесы')

@section('content')
    <div class="d-flex flex-column businesses-page">
        <div class="head-title">Бизнесы в США на продажу</div>
        <div class="saver">
            <form method="get" action="{{ route('businesses.index', app()->getLocale()) }}">
                <div class="d-flex flex-lg-row flex-wrap justify-content-between filter">
                    <div class="city order-lg-1 order-3">
                        <select id="city" multiple name="city[]">
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex fb order-lg-2 order-1">
                        <input type="checkbox" class="btn-check" name="fb[]" value="Франшиза" id="franchise">
                        <label class="btn btn-primary franchise" for="franchise">Франшиза</label>
                        <input type="checkbox" class="btn-check" name="fb[]" value="Бизнес" id="business">
                        <label class="btn btn-primary business" for="business">Бизнес</label>
                    </div>
                    <div class="industry order-lg-3  order-4">
                        <select id="industry" multiple name="industry[]">
                            @foreach($industries as $industry)
                                <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="price order-lg-4 order-2">
                        <button class="btn btn-primary price-zamanka" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapsePrice" aria-expanded="false" aria-controls="collapsePrice">
                            Цена
                        </button>
                        <div class="collapse" id="collapsePrice">
                            <div class="d-flex flex-lg-row justify-content-between card card-body">
                                <input class="form-control price-control" type="text" placeholder="От $" id="price_from"
                                       name="price_from" value="{{ request()->price_from }}"
                                       aria-label="default input example">
                                <input class="form-control price-control" type="text" placeholder="До $" id="price_to"
                                       name="price_to" value="{{ request()->price_to }}"
                                       aria-label="default input example">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="search-btn order-lg-5 order-5">Найти</button>
                </div>
            </form>
        </div>
        <div class="d-flex flex-column businesses">
            @forelse($businesses as $business)
                <div class="business d-flex flex-lg-row flex-column justify-content-between">
                    <div class="left d-flex flex-lg-row flex-column">
                        <img src="{{ asset('storage/'. $business->preview) }}" class="preview" alt="" loading="lazy">
                        <div class="d-flex flex-column info">
                            <div class="title">{!! $business->title !!}</div>
                            @if(!empty($business->city->name))
                                <div class="place">{!! $business->city->name !!}</div>
                            @endif
                            <div class="description mb-3">{!! $business->short_description !!}</div>
							<!--<p>{!! $business->created_at !!}</p>-->
							<p>{!! date('d.m.Y', strtotime($business->created_at)) !!}</p>
                        </div>
                    </div>
                    <div class="right d-flex flex-column align-items-end">
                        <div
                            class="box d-flex flex-lg-column flex-row justify-content-between align-items-lg-end align-items-center">
                            <div class="price">${!! number_format($business->asking_price , 0, ',', ',') !!}</div>
                            <div class="cash-flow">Cash flow:
                                ${!! number_format($business->cash_flow , 0, ',', ',') !!}</div>
                        </div>
                        <a href="{{ route('businesses.show', [app()->getLocale(), $business->slug]) }}" class="mt-lg-auto">
                            <div class="details">Подробнее</div>
                        </a>
                    </div>
                </div>
            @empty
                <div class="text-left">
                    По вашему запросу ничего не найдено
                </div>
            @endforelse
        </div>
        {{ $businesses->appends(request()->input())->links() }}
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            checkInput();
        })

        $('#price_from').on('input change', checkInput)

        function checkInput() {
            $('#price_from').each(function () {
                if ($(this).val() !== '') {
                    $('.price-zamanka').addClass('bcb')
                } else {
                    $('.price-zamanka').removeClass('bcb')
                }
            })
        }
    </script>
@endsection
