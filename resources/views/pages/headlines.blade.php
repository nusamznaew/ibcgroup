@extends('layouts.layout')

@section('title', 'Новости')

@section('content')
    <div class="headlines-page d-flex flex-column justify-content-between">
        <div class="left">
            @foreach($headlines as $headline)
                <div class="breadcrumbs">
                    <p class="d-inline date">{{ $headline->created_at->format('d M Y') }}</p>
                </div>
                <a href="{{ route('headlines.show', [app()->getLocale(), $headline->slug]) }}"><h2>{{ $headline->title }}</h2></a>
                <div class="d-flex flex-column description">
                    {!! Str::limit($headline->description, 500) !!}
                </div>
            @endforeach
            <div class="similar-headlines">
                @foreach($similarHeadlines as $headline)
                    <div class="box d-flex flex-row">
                        <a href="{{ route('headlines.show', [app()->getLocale(), $headline->slug]) }}">
                            <div class="img"
                                 style="background-image: url('{{ asset('storage/'. $headline->image) }}')"></div>
                        </a>
                        <div class="d-flex flex-column">
                            <a class="title"
                               href="{{ route('headlines.show', [app()->getLocale(), $headline->slug]) }}">{{ $headline->title }}</a>
                            <div class="short">{!! $headline->short !!}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
@endsection
