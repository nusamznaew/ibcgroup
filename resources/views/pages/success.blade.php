<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">
    <title>Document</title>
</head>
<style>
    html, body {
        height: 100%;
    }

    .big-txt {
        font-family: 'Museosanscyrl-Bold', sans-serif;
        font-size: 48px;
        line-height: 56px;
        letter-spacing: 0.05em;

        color: #484848;

        margin-bottom: 31px;
    }

    .small-txt {
        font-family: 'Museosanscyrl-Light', sans-serif;
        font-size: 24px;
        line-height: 28px;
        text-align: center;

        color: #484848;

        margin-bottom: 40px;
    }

    a {
        text-decoration: none;
    }

    a > div{
        width: 270px;
        height: 56px;

        display: flex;
        justify-content: center;
        align-items: center;

        text-decoration: none;

        background: #DA1E26;
        border-radius: 8px;

        font-family: 'Museosanscyrl-Bold', sans-serif;
        font-size: 16px;
        line-height: 19px;

        color: #FFFFFF;
    }
</style>
<body>
<div class="d-flex flex-column justify-content-center align-items-center h-100">
    <div class="big-txt">Спасибо</div>
    <div class="small-txt">Ваша заявка отправлена. <br>
        Наш специалист перезвонит к Вам в рабочее время.</div>
    <a href="/">
       <div>Вернуться на главную</div>
    </a>
</div>
</body>
</html>
