@extends('layouts.layout')

@section('title', 'СМИ о нас')

@section('extra-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
          integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="{{ asset('assets/css/youCover.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center mb-3">СМИ о нас</h2>
            <div class="row mass-media">
                @foreach($medias as $media)
                    <div class="col-md-4 col-12 video">

                        <!-- Id youtube видео -->
                        <div data-youcover data-fancybox data-id='{{ $media->link }}' data-image='{{ asset('storage/'. $media->preview ) }}' data-rel='galery'></div>

                        <!-- Описание видео -->
                        <p>{!! $media->description !!}</p>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
            integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src='{{ asset('assets/js/youCover.js') }}'></script>
@endsection
