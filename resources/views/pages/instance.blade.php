@extends('layouts.layout')

@section('title', $instance->title)

@section('content')
    <div class="instance-page d-flex flex-column justify-content-between">
        <div class="background mb-4" style="background-image: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0)), url('{{ asset('storage/'. $instance->image) }}')">
            <div class="breadcrumbs">
                <a class="link" href="{{ route('instances.index', app()->getLocale()) }}">{{ $instance->category }}</a>
                <p class="d-inline date">{{ $instance->created_at->format('d M Y') }}</p>
            </div>
            <h2>{{ $instance->title }}</h2>
        </div>
        <div class="d-flex flex-lg-row flex-column justify-content-between">
            <div class="left">
                <div class="d-flex flex-column description">
                    <div class="description">
                        {!! $instance->description !!}
                    </div>
                </div>
                <div id="disqus_thread"></div>
            </div>
            <div class="right similar">
                @foreach($similarInstances as $instance)
                    <div class="box d-flex flex-row">
                        <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}">
                            <div class="img" style="background-image: url('{{ asset('storage/'. $instance->image) }}')"></div>
                        </a>
                        <div class="d-flex flex-column">
                            <a class="title" href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}">{{ Str::limit($instance->title, 50) }}</a>
                            <div class="date">{!! $instance->created_at->format('d M Y') !!}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://ibcportal-kz-1.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection
