@extends('layouts.layout')

@section('title', 'Мастер классы')

@section('content')
    <div class="d-flex flex-wrap masterclasses-page">
        @foreach($masterclasses as $masterclass)
            <div class="d-flex flex-column box">
                <a href="{{ route('masterclasses.show', [app()->getLocale(), $masterclass->slug]) }}" class="img">
                    <div class="picha" style="background-image: url('{{ asset('storage/'. $masterclass->image) }}')"></div>
                </a>
                <a href="{{ route('masterclasses.show', [app()->getLocale(), $masterclass->slug]) }}" class="big-text">{{ $masterclass->title }}</a>
				@if(!empty($masterclass->date))
					<div><i class="bi bi-calendar-event"></i> {{ $masterclass->date->translatedFormat('jS F') }} в {{ $masterclass->date->format('H:i') }}</div>
				@endif
                <div><i class="bi bi-geo-alt"></i> {{ $masterclass->place }}</div>
				@if(!empty($masterclass->price))
                	<div><i class="bi bi-cash"></i>{{ $masterclass->price }} тг</div>
				@endif
            </div>
        @endforeach
    </div>
@endsection

@section('extra-js')

@endsection
