@extends('layouts.welcome')

@section('content')
    <section id="open_biz">
        <div class="container">
            <div class="d-flex flex-column">
                <h1>Откройте ваш бизнес в США</h1>
                <p class="small-txt">Поможем составить бизнес-план, <br> зарегистрировать компанию и <br
                        class="d-lg-none d-block"> начать продавать</p>
                <div class="d-flex flex-lg-row flex-column request justify-content-between">
                    <div class="video">
                        <div data-youcover data-image='{{ asset('img2/kurmet.jpg') }}' data-fancybox
                             data-id='ZoZW-gVrx3Y' data-rel='galery'></div>
                    </div>
                    <div class="form d-flex flex-column">
                        <div class="big-txt">Опишите ваш запрос в двух словах.</div>
                        <div class="small-txt">Наш консультант свяжется с вами, чтобы назначить бесплатную
                            видеоконсультацию.
                        </div>
                        <form action="" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-floating">
                            <textarea class="form-control" name="comment" placeholder="Leave a comment here"
                                      id="floatingTextarea" required></textarea>
                                <label for="floatingTextarea">Ваш запрос</label>
                            </div>
                            <input class="form-control" name="name" type="text" placeholder="Ваше имя"
                                   aria-label="default input example" required>
                            <input class="form-control" name="email" type="email" placeholder="Ваше e-mail"
                                   aria-label="default input example" required>
                            <button type="submit" class="submit-button">Получить консультацию</button>
                        </form>
                        <small>Вы даете согласие на обработку персональных данных и соглашаетесь c политикой
                            конфиденциальности.</small>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="parallax">
        <video playsinline autoplay muted loop
               src="https://res.cloudinary.com/nusamznaew/video/upload/v1633347968/bg-video4_hdhtvj.mp4">
        </video>
    </section>
    <section id="service_list">
        <div class="container">
            <div class="d-flex flex-column">
                <h2>Список услуг для бизнеса в США</h2>
                <div class="d-flex flex-lg-row flex-column service-list justify-content-between">
                    <div style="background-image: url('{{ asset('img2/business1.jpg') }}')">
                        Регистрация компании в США
                    </div>
                    <div style="background-image: url('{{ asset('img2/business2.jpg') }}')">
                        Бизнес-представительство в США
                    </div>
                    <div style="background-image: url('{{ asset('img2/business3.jpg') }}')">
                        Помощь с ведением бизнеса в США
                    </div>
                    <div style="background-image: url('{{ asset('img2/business4.jpg') }}')">
                        Торговое представительство в США
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="process">
        <h2>Процесс нашей работы</h2>
        <div class="d-flex justify-content-center process">
            <div class="d-flex flex-column justify-content-between order-lg-1 order-2">
                <div class="d-flex flex-column align-items-lg-center align-items-start box">
                    <img src="{{ asset('img2/email.jpg') }}" class="d-lg-block d-none" alt="">
                    <div class="big-txt"><img src="{{ asset('img2/email.jpg') }}" class="d-lg-none d-inline" alt="">Ваш
                        запрос
                    </div>
                    <div class="small-txt">Получаем ваш запрос через нашу <a href="#">форму обратной связи</a> или
                        другим
                        путем
                    </div>
                </div>
                <div class="d-flex flex-column align-items-lg-center align-items-start box up">
                    <img src="{{ asset('img2/key.jpg') }}" class="d-lg-block d-none" alt="">
                    <div class="big-txt"><img src="{{ asset('img2/key.jpg') }}" class="d-lg-none d-inline" alt="">Контракт
                        и
                        расчет
                    </div>
                    <div class="small-txt">Составляем контракт об оказании услуг, получаем от вас оплату</div>
                </div>
                <div class="d-lg-none d-flex flex-column align-items-lg-center align-items-start box">
                    <img src="{{ asset('img2/camera.jpg') }}" class="d-lg-block d-none" alt="">
                    <div class="big-txt"><img src="{{ asset('img2/camera.jpg') }}" class="d-lg-none d-inline" alt="">Видеоконсультация
                    </div>
                    <div class="small-txt">Проводим ознакомительный звонок с нашим агентом или президентом компании
                    </div>
                </div>
                <div class="d-lg-none d-flex flex-column align-items-lg-center align-items-start box">
                    <img src="{{ asset('img2/tick.jpg') }}" class="d-lg-block d-none" alt="">
                    <div class="big-txt"><img src="{{ asset('img2/tick.jpg') }}" class="d-lg-none d-inline" alt="">Исполнение
                    </div>
                    <div class="small-txt">Выполняем работу в обговоренный срок, демонстрируем результаты</div>
                </div>
            </div>
            <img class="stepline order-lg-2" src="{{ asset('img2/stepline.jpg') }}" alt="">
            <div class="d-lg-flex d-none flex-column justify-content-between order-lg-3 order-1">
                <div class="d-flex flex-column align-items-lg-center align-items-start box down">
                    <img src="{{ asset('img2/camera.jpg') }}" alt="">
                    <div class="big-txt">Видеоконсультация</div>
                    <div class="small-txt">Проводим ознакомительный звонок с нашим агентом или президентом компании
                    </div>
                </div>
                <div class="d-flex flex-column align-items-lg-center align-items-start box">
                    <img src="{{ asset('img2/tick.jpg') }}" alt="">
                    <div class="big-txt">Исполнение</div>
                    <div class="small-txt">Выполняем работу в обговоренный срок, демонстрируем результаты</div>
                </div>
            </div>
        </div>
    </section>
    <section id="faq">
        <div class="d-flex flex-column container align-items-center">
            <h2>Часто задаваемые вопросы</h2>
            <div class="accordion" id="faqAccordion">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Какова стоимость ваших услуг?
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Цена варьируется в зависимости от вашего запроса. К нам обращаются клиенты с совершенно
                            разнообразными ситуациями, целями и объемом бюджета.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Сколько стоит открыть бизнес в Америке?
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Напишите нам, какой бизнес вы хотели бы открыть. Мы предоставим расценки на услуги в рамках
                            первоначальной консультации. Заполните форму, чтобы продолжить.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Хочу открыть компанию в США. Что мне для этого нужно?
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Мы составили вопросник со списком часто задаваемых вопросов при открытии новой компании в
                            США. Вы можете его скачать бесплатно, если заполните форму по ссылке.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Можно ли открыть бизнес в США дистанционно?
                        </button>
                    </h2>
                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Да! Обратитесь к нам за бесплатной консультацией.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingFive">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Хочу получить визу в США. Вы можете мне помочь?
                        </button>
                    </h2>
                    <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Нет, мы не предоставляем услуги визового агентства.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Хочу найти работу в США.
                        </button>
                    </h2>
                    <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Мы не занимаемся поиском работы в США. Если вы собираетесь начать новый бизнес или купить
                            существующий, мы можем вам помочь.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingSeven">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            Зачем мне услуги консультанта?
                        </button>
                    </h2>
                    <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Разобраться в тонкостях ведения бизнеса в другой стране может быть непросто - для этого
                            потребуется знание английского языка, свободное время и внимательность в чтении документов.
                            Консультант - в данном случае это представитель Catlett, Inc. - делает все это за вас.
                            <br>
                            Мы - американская компания, и наши сотрудники в Америке уже предоставляли указанные выше
                            услуги для других бизнесов. Мы знаем про "подводные камни" в ведении бизнеса в США.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingEight">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            Какие еще услуги вы предоставляете?
                        </button>
                    </h2>
                    <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Три сферы деятельности: инвестиционная программа иммиграции EB-5, брокерская деятельность, и
                            консультации по подбору недвижимости в США.
                            <br>
                            <br>
                            1. Мы консультируем инвесторов, заинтересованных в иммиграции в США по программе EB-5.
                            Требуемый размер инвестиций составляет 900 тысяч долларов США. Наш партнер Pine State
                            Regional Center предлагает проект Big River Steel Phase II, инвестиции в который можно
                            использовать для подачи на визу EB-5. Подробнее здесь. <br><br>
                            2. Мы являемся посредником в международной торговле. Наша организация входит в реестр
                            поставщиков товаров и услуг для ООН. Если вас интересует покупка и продажа товаров за
                            границей, ознакомьтесь с нашими текущими партнерами. <br><br>
                            3. Мы занимаемся подбором инвестиционной недвижимости в США, как для получения последующей
                            прибыли, так и для переезда и последующей жизни.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingNine">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                            Не нашел свою услугу в списке.
                        </button>
                    </h2>
                    <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine"
                         data-bs-parent="#faqAccordion">
                        <div class="accordion-body">
                            Опишите ваш запрос через форму обратной связи или свяжитесь с нами любым удобным способом.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="test" >
        <header>
            <small>Виза ЕВ-1А тест</small>
        </header>
        <div class="d-flex flex-column align-items-center">
            <section data-quiz-item>
                <div class="question">1. Вы работаете в области науки, искусства, образования, бизнеса или спорта?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">2. Вы получали национальные или международные призы, грамоты, награды за выдающееся способности в вашей профессии?
                </div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">3. Вы являетесь членом профессиональных ассоциаций, членами которых могут стать только люди с уникальными способностями (в отличие от всех кто просто внес членский сбор)?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">4. О вашей работе есть публикации в профессиональных изданиях или иных источниках медии, известных на национальном или международном уровне?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">5. Вы можете продемонстрировать как своей работой вы сделали вклад в индустрию, например, написанные статьи, создание компьютерных программ и приложений, выдвинутые и принятые идеи, руководство определенными группами сотрудников, т. д.?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">6. Вы можете собрать письма-рекомендации от людей, которые на основании их образования, профессиональной позиции, занимаемой должности могут считаться экспертами в вашей индустрии?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">7. Публиковали ли ваши профессиональные статьи или давали интервью СМИ, которые известны на национальном или международном уровне?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">8. Получаете ли вы высокую зарплату или иную компенсацию (роялти, прибыль от продажи билетов, ваших продуктов, т.д.) по сравнению со средними представителями профессии?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">9. Входили ли вы когда-либо жюри, панель экспертов, выступали в качестве оценьщика работы других людей в вашей индустрии?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <section data-quiz-item>
                <div class="question">10. Занимаете ли вы важную должности в организации с заслуженной репутацией на национальном или междунаровном уровне?</div>
                <div class="choices" data-choices='["Да","Нет"]'>
                </div>
            </section>
            <div class="submit">
                <button id="emc-submit">Показать результат</button>
            </div>
            <div id="emc-score"></div>
        </div>
        <!--
        <div class="wrap">
            <div class="row">
                <section data-quiz-item>
                    <div class="question">1. Вы работаете в области науки, искусства, образования, бизнеса или спорта?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
                <section data-quiz-item>
                    <div class="question">2. Вы получали национальные или международные призы, грамоты, награды за выдающееся способности в вашей профессии?
                    </div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
            </div>
            <div class="row">
                <section data-quiz-item>
                    <div class="question">3. Вы являетесь членом профессиональных ассоциаций, членами которых могут стать только люди с уникальными способностями (в отличие от всех кто просто внес членский сбор)?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
                <section data-quiz-item>
                    <div class="question">4. О вашей работе есть публикации в профессиональных изданиях или иных источниках медии, известных на национальном или международном уровне?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
            </div>
            <div class="row">
                <section data-quiz-item>
                    <div class="question">5. Вы можете продемонстрировать как своей работой вы сделали вклад в индустрию, например, написанные статьи, создание компьютерных программ и приложений, выдвинутые и принятые идеи, руководство определенными группами сотрудников, т. д.?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
                <section data-quiz-item>
                    <div class="question">6. Вы можете собрать письма-рекомендации от людей, которые на основании их образования, профессиональной позиции, занимаемой должности могут считаться экспертами в вашей индустрии?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
            </div>
            <div class="row">
                <section data-quiz-item>
                    <div class="question">7. Публиковали ли ваши профессиональные статьи или давали интервью СМИ, которые известны на национальном или международном уровне?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
                <section data-quiz-item>
                    <div class="question">8. Получаете ли вы высокую зарплату или иную компенсацию (роялти, прибыль от продажи билетов, ваших продуктов, т.д.) по сравнению со средними представителями профессии?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
            </div>
            <div class="row last">
                <section data-quiz-item>
                    <div class="question">9. Входили ли вы когда-либо жюри, панель экспертов, выступали в качестве оценьщика работы других людей в вашей индустрии?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
                <section data-quiz-item>
                    <div class="question">10. Занимаете ли вы важную должности в организации с заслуженной репутацией на национальном или междунаровном уровне?</div>
                    <div class="choices" data-choices='["Да","Нет"]'>
                    </div>
                </section>
            </div>
            <div class="submit">
                <button id="emc-submit">Показать результат</button>
            </div>
        </div>
        <div id="emc-score"></div>
        -->
    </section>
    <section id="want">
        <div class="container block d-flex flex-lg-row flex-column justify-content-between">
            <div class="">Хотите бесплатно получить материалы по <br class="d-lg-block d-none">
                ведению бизнеса в США? Заполните форму по <br class="d-lg-block d-none">
                ссылке, и мы вам их
                пришлем моментально.
            </div>
            <a href="#contact">
                <div>Подробнее</div>
            </a>
        </div>
    </section>
    <section id="contact">
        <div class="container d-flex flex-column align-items-lg-center align-items-start">
            <h2>Контакты</h2>
            <div class="d-flex flex-lg-row flex-column justify-content-between content">
                <div class="d-flex flex-column">
                    <h3>Наши адреса</h3>
                    <div class="box d-flex flex-column">
                        <div class="big-txt">Главный офис в США</div>
                        <p class="small-txt">1920 E Hallandale Beach Blvd, USA</p>
                        <a href="tel:+1 (332) 209-93-38" class="small-txt phone">+1 (754) 581-73-52</a>
                        <a class="small-txt email">info@usanaruss.com</a>
                    </div>
                    <div class="box d-flex flex-column">
                        <div class="big-txt">Казахстан, Россия и СНГ</div>
                        <p class="small-txt">г.Алматы, мкр. Коктем 2, д.22. 4 эт.</p>
                        <a href="tel:+7 (727) 123-45-67" class="small-txt phone">+7 (727) 123-45-67</a>
                        <a href="tel:+7 (701) 123-45-67" class="small-txt phone">+7 (701) 123-45-67</a>
                        <a class="small-txt email">help@ibcg.kz</a>
                    </div>
                </div>
                <div class="form d-flex flex-column">
                    <h3>Связаться с нами</h3>
                    <form action="" method="post">
                        @csrf
                        <input class="form-control" name="email" type="email" placeholder="Ваше e-mail"
                               aria-label="default input example" required>
                        <input class="form-control" name="name" type="text" placeholder="Ваше имя"
                               aria-label="default input example" required>
                        <input class="form-control" name="phone" type="tel" placeholder="Ваш телефон"
                               aria-label="default input example" required>
                        <div class="form-floating">
                            <textarea class="form-control" name="comment" placeholder="Leave a comment here"
                                      id="floatingTextarea" required></textarea>
                            <label for="floatingTextarea">Comments</label>
                        </div>
                        <button type="submit" class="submit-button">Получить консультацию</button>
                    </form>
                    <small>Вы даете согласие на обработку персональных данных и соглашаетесь c политикой
                        конфиденциальности.</small>
                </div>
            </div>
        </div>
    </section>
@endsection
