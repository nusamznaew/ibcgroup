@extends('layouts.layout')

@section('title', 'Кейсы')

@section('description', 'Вдохновляющие истории казахстанцев, которые следуя за своей мечтой, выбирают путь получения зарубежного образования в США.')

@section('keywords', 'кейсы, вдохновляющиеисторииказахстанцев, казахивсша, историиказаховвсша, зарубежноеобразованиевсша, образованиевсша, вдохновляющиеистории, учебазаграницей, мотивирующиеистории, нашивсша')

@section('content')
    <div class="d-flex flex-wrap instances-page">
        @foreach($instances as $instance)
            <div class="d-flex flex-column box">
                <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}" class="img">
					<div class="picha" style="background-image: url('{{ asset('storage/'. $instance->image) }}')"></div>
				</a>
                <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}" class="edu">{{ $instance->category }}</a>
                <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}" class="big-text">{{ Str::limit($instance->title, 60) }}</a>
                <div class="d-flex small-text">
                    <p>{{ $instance->created_at->format('d M Y') }}</p>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('extra-js')

@endsection
