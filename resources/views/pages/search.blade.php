@extends('layouts.layout')

@section('title', 'Мастер классы')

@section('content')
    <div class="card">
        <div class="card-header">Поиск</div>
        <div class="card-body">
            <div class="card">
                <div class="card-header"><b>найдено {{ $searchResults->count() }} результатов для "{{ request('query') }}"</b>
                </div>
                <div class="card-body">

                    @foreach($searchResults->groupByType() as $type => $modelSearchResults)

                        @foreach($modelSearchResults as $searchResult)
                            <ul>
                                <li><a href="{{ $searchResult->url }}">{{ $searchResult->title }}</a></li>
                            </ul>
                        @endforeach
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

@endsection
