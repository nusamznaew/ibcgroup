@extends('layouts.layout')

@section('title', $business->title)

@section('content')
    <div class="business-page d-flex flex-row justify-content-between">
        <div class="business-info">
            <h1>{!! $business->title !!}</h1>
            <div class="place">{!! $business->place !!}</div>
            <div class="d-flex flex-column">
                <div id="carouselBusinessIndicators" class="carousel slide business-carousel" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        @foreach(json_decode($business->banner, true) as $banner)
                            @if($loop->index === 0)
                                <button type="button" data-bs-target="#carouselMainIndicators"
                                        data-bs-slide-to="{{ $loop->index }}"
                                        class="active" aria-current="true"
                                        aria-label="Slide {{ $loop->index }}"></button>
                            @else
                                <button type="button" data-bs-target="#carouselMainIndicators"
                                        data-bs-slide-to="{{ $loop->index }}"
                                        aria-label="Slide {{ $loop->index }}"></button>
                            @endif
                        @endforeach
                    </div>
                    <div class="carousel-inner">
                        @foreach(json_decode($business->banner, true) as $banner)
                            @if($loop->index === 0)
                                <div class="carousel-item active">
                                    <img src="{{ Voyager::image($banner) }}" class="d-block w-100"
                                         alt="..." loading="lazy">
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img src="{{ Voyager::image($banner) }}" class="d-block w-100"
                                         alt="..." loading="lazy">
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselBusinessIndicators"
                            data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselBusinessIndicators"
                            data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
                <div class="price-info">
                    <div class="line-1 borderito d-flex flex-lg-row flex-column justify-content-between">
                        <div class="left d-flex justify-content-between">
                            <div class="f-18">Запрашиваемая цена:</div>
                            <div class="big-price size">
                                ${!! number_format($business->asking_price, 0, ',', ',') !!}</div>
                        </div>
                        <div class="right d-flex justify-content-between">
                            <div class="f-18">Денежный поток:</div>
                            <div class="big-price size">${!! number_format($business->cash_flow, 0, ',', ',') !!}</div>
                        </div>
                    </div>
                    <div class="line-2 borderito">
                        <div class="d-flex justify-content-between">
                            <div class="f-15">Прибыль до уплаты процентов, налогов, износа и амортизации (EBITDA)</div>
                            <div class="f-16 size">${!! number_format($business->ebitda, 0, ',', ',') !!}</div>
                        </div>
                    </div>
                    <div class="line-3 borderito">
                        <div class="d-flex justify-content-between">
                            <div class="f-15">Оборудование/крепления/фурнитура (FF&E):</div>
                            <div class="f-16 size">${!! number_format($business->ffe_price, 0, ',', ',') !!}</div>
                        </div>
                    </div>
                    <div
                        class="line-4 borderito d-flex flex-lg-row flex-column justify-content-between align-items-center">
                        <div class="left d-flex justify-content-between">
                            <div class="f-16">Валовой доход:</div>
                            <div class="f-16 size">${!! number_format($business->gross_income, 0, ',', ',') !!}</div>
                        </div>
                        <div class="divider"></div>
                        <div class="right d-flex justify-content-between">
                            <div class="f-16">Аренда:</div>
                            <div class="f-16 size">${!! number_format($business->rent, 0, ',', ',') !!}</div>
                        </div>
                    </div>
                    <div
                        class="line-5 borderito d-flex flex-lg-row flex-column justify-content-between align-items-center">
                        <div class="left d-flex justify-content-between">
                            <div class="f-16">Инвентарь:</div>
                            <div class="f-16 size">${!! number_format($business->inventory_price, 0, ',', ',') !!}</div>
                        </div>
                        <div class="divider"></div>
                        <div class="right d-flex justify-content-between">
                            <div class="f-16">Основан:</div>
                            <div class="f-16 size">{!! $business->founded !!} г.</div>
                        </div>
                    </div>
                </div>
                <div class="business-description borderito">
                    <div class="head-title">Описание бизнеса</div>
                    <div class="title">{!! $business->title !!}</div>
                    <div class="description">
                        {!! $business->description !!}
                    </div>
                </div>
                <div class="detailed-info">
                    <div class="head-title">Детальная информация</div>
                    <div class="line-1 borderito">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <div class="f-16 left">Локация:</div>
                            <div class="f-16 right">{!! $business->place !!}</div>
                        </div>
                    </div>
                    <div class="line-2 borderito">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <div class="f-16 left">Инвентарь:</div>
                            <div class="f-16 right">{!! $business->inventory !!}</div>
                        </div>
                    </div>
                    <div class="line-3 borderito">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <div class="f-15 left">Оборудование/<br class="d-lg-none d-block">крепления/<br
                                    class="d-lg-none d-block">фурнитура (FF&E):
                            </div>
                            <div class="f-15 right">{!! $business->ffe !!}</div>
                        </div>
                    </div>
                    <div class="line-4 borderito">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <div class="f-15 left">Сооружения/объекты:</div>
                            <div class="f-15 right">{!! $business->object !!}</div>
                        </div>
                    </div>
                    <div class="line-5 borderito">
                        <div class="d-flex flex-row justify-content-between align-items-start">
                            <div class="f-15 left">Поддержка и обучение:</div>
                            <div class="f-15 right">
                                {!! $business->support !!}
                            </div>
                        </div>
                    </div>
                    <div class="line-6 borderito">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <div class="f-15 left">Причина продажи:</div>
                            <div class="f-15 right">{!! $business->reason_for_selling !!}</div>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Оставить заявку</h3>
            <form action="{{ route('businesses.send') }}" method="post" class="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="mb-3">
                    <label for="business-name" class="form-label">Имя</label>
                    <input name="name" type="text" class="form-control" id="business-name" placeholder="Имя" required>
                </div>
                <div class="mb-3">
                    <label for="business-email" class="form-label">E-mail</label>
                    <input name="email" type="email" class="form-control" id="business-email" placeholder="E-mail"
                           required>
                </div>
                <div class="mb-3">
                    <label for="business-phone" class="form-label">Телефон</label>
                    <input name="phone" type="tel" class="form-control phone" id="business-phone"
                           placeholder="Телефон" required>
                </div>
                <input name="business" type="text" hidden value="{{ $business->title }}" aria-label="default">
                <div class="mb-3">
                    <button class="btn submit-button" type="submit">Оставить заявку</button>
                </div>
            </form>
        </div>
        <div class="other-business">
            <div class="head-title">Похожие объявления</div>
            <div class="d-flex flex-column">
                @foreach($similarBusinesses as $similarBusiness)
                    <a href="{{ route('businesses.show', $similarBusiness->slug) }}">
                        <div class="simi">
                            <img src="{{ asset('storage/'. $similarBusiness->preview) }}" alt="">
                            <div class="info">
                                <a href="{{ route('businesses.show', $similarBusiness->slug) }}">
                                    <div class="title">{!! $similarBusiness->title !!}</div>
                                </a>
                                <div class="place">{!! $similarBusiness->place !!}</div>
                                <div class="price">Запрашиваемая цена: ${!!number_format($similarBusiness->asking_price , 0, ',', ',') !!}</div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

@endsection
