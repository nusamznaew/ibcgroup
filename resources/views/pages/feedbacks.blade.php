@extends('layouts.layout')

@section('title', 'Отзывы')

@section('content')
    <div class="d-flex flex-wrap feedbacks-page">
        @foreach($feedbacks as $feedback)
            <div class="d-flex flex-column box">
                <a href="{{ route('feedbacks.show', [app()->getLocale(), $feedback->slug]) }}" class="img">
				    <div class="picha" style="background-image: url('{{ asset('storage/'. $feedback->image) }}')"></div>
					<!-- <img src="{{ asset('storage/'. $feedback->image) }}" alt=""> -->
				</a>
                <a href="{{ route('feedbacks.show', [app()->getLocale(), $feedback->slug]) }}" class="edu">{{ $feedback->name }}</a>
                <a href="{{ route('feedbacks.show', [app()->getLocale(), $feedback->slug]) }}">
                    <div class="big-text">
                        <p>
                            {!! Str::limit($feedback->title, 60) !!}
                        </p>
                    </div>
                </a>
                <div class="d-flex small-text">
                    <p>{{ $feedback->created_at->format('d M Y') }}</p>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('extra-js')

@endsection
