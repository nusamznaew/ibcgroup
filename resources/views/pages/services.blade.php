@extends('layouts.layout')

@section('title', 'Наши услуги')

@section('description', 'В IBC Group более 13 направлений в сфере образования и трудоустройства за рубежом.')

@section('keywords', 'учебавсша, учисьвсша, работавсша, найтиработувсша, трудоустройствовсша, какнайтиработувсша, гдеучитьсявсша, вузысша, кудапоступитьвсша, образованиезарубежом')

@section('content')
    <div class="page-services">
        <div class="d-flex flex-lg-row flex-column headcrab align-items-center justify-content-between">
            <div class="d-flex big-txt align-items-center">
                Услуги
            </div>
            <div class="small-txt">
                Люди - главная ценность компании. Мы верим в каждого. У нас люди находят себя и свое призвание. А мы
                вдохновляем новыми горизонтами и поддерживаем во всех начинаниях.
            </div>
        </div>
        <div class="d-flex flex-wrap justify-content-between services">
            <a href="https://one-ibc.kz/" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service1.jpg') }}" loading="lazy" alt="">
            </a>
            <a href="https://trip.usanaruss.com/" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service2.jpg') }}" loading="lazy" alt="">
            </a>
            <a href="https://ibcg-usa.kz/" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service3.jpg') }}" loading="lazy" alt="">
            </a>
            <a href="https://usanaruss.com/" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service4.jpg') }}" loading="lazy" alt="">
            </a>
            <a href="https://trip.usanaruss.com/" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service5.jpg') }}" loading="lazy" alt="">
            </a>
			<a href="https://biz.usanaruss.com/" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service6.jpg') }}" loading="lazy" alt="">
            </a>
			<a href="https://ibcg.kz/businesses" class="s-link" target="_blank" rel="noopener noreferrer">
                <div class="detailed">
                    Подробнее
                </div>
                <img src="{{ asset('img/service7.jpg') }}" loading="lazy" alt="">
            </a>
        </div>
    </div>
@endsection

@section('extra-js')

@endsection
