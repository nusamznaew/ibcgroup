@extends('layouts.layout')

@section('title', 'Что такое IBC Group')

@section('description', 'Международная команда экспертов в сфере образования в США. Головной офис находится в Майами.')

@section('keywords', 'ibcg, ibcgroup, учебавсша, работавсша, workstudyusa, studyinusa, workinusa, usa, образованиевсша, studyusa')

@section('extra-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
          integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="{{ asset('assets/css/youCover.css') }}">
@endsection

@section('content')
    <div class="ibc-group-is">
        <div class="section-1 d-flex flex-column align-items-center">
            <h2>IBC GROUP - ЭТО</h2>
            <div class="d-flex flex-wrap justify-content-center list">
                <div class="element d-flex flex-column align-items-center">
                    <object type="image/svg+xml" data="/img/winner.svg"></object>
                    <p class="big-txt">№1</p>
                    <p class="small-txt">по образованию в США</p>
                </div>
                <div class="element d-flex flex-column align-items-center">
                    <object type="image/svg+xml" data="/img/graduation-cap.svg"></object>
                    <p class="big-txt">> 800</p>
                    <p class="small-txt">отправлены <br> на учебу</p>
                </div>
                <div class="element d-flex flex-column align-items-center">
                    <object type="image/svg+xml" data="/img/office.svg"></object>
                    <p class="big-txt">12</p>
                    <p class="small-txt">офисов <br> по миру</p>
                </div>
                <div class="element d-flex flex-column align-items-center">
                    <object type="image/svg+xml" data="/img/manager.svg"></object>
                    <p class="big-txt">95</p>
                    <p class="small-txt">работает <br> в штате</p>
                </div>
                <div class="element d-flex flex-column align-items-center">
                    <object type="image/svg+xml" data="/img/office-party.svg"></object>
                    <p class="big-txt">2018</p>
                    <p class="small-txt">создана компания</p>
                </div>
            </div>
        </div>
        <div class="section-2 d-lg-block d-none">
            <div class="d-flex top">
                <img src="{{ asset('img/pic1.jpg') }}" alt="" loading="lazy">
                <img src="{{ asset('img/pic2.jpg') }}" alt="" loading="lazy">
                <img src="{{ asset('img/pic3.jpg') }}" alt="" loading="lazy">
                <img src="{{ asset('img/pic4.jpg') }}" alt="" loading="lazy">
                <img src="{{ asset('img/pic5.jpg') }}" alt="" loading="lazy">
            </div>
            <div class="bottom d-flex">
                <div class="box d-flex flex-column">
                    <img src="{{ asset('img/pic6.jpg') }}" alt="" loading="lazy">
                    <img src="{{ asset('img/pic7.jpg') }}" alt="" loading="lazy">
                </div>
                <img src="{{ asset('img/pic8.jpg') }}" alt="" loading="lazy">
                <div class="box-2">
                    <h3>Follow your dreams</h3>
                    <p>Мы верим, что всё в жизни человека начинается с мечты. Любая достигнутая цель, любое достижение -
                        всё это когда-то было мечтой. Мы верим, что хорошее образование помогает найти себя и
                        реализовать свои мечты. Поэтому мы создали IBC Group - сообщество людей, которые следуют за
                        своей мечтой. Которые выбирают путь к мечте через зарубежное образование.</p>
                    <a class="watch-video video-btn" href="#" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/bfWKFGJdPR4" data-bs-target="#modalVideo">Смотреть видео <i class="bi bi-play-fill"></i></a>
                </div>
            </div>
        </div>
        <div class="section-2-mobile d-lg-none d-block">
            <div class="d-flex flex-column">
                <div class="d-flex top">
                    <div class="left">
                        <img src="{{ asset('img/pic1.jpg') }}" class="" alt="..." loading="lazy">
                    </div>
                    <div class="right">
                        <img src="{{ asset('img/pic2.jpg') }}" class="" alt="..." loading="lazy">
                    </div>
                </div>
                <div class="d-flex justify-content-between center">
                    <div class="left d-flex flex-column position-relative">
                        <div class="first"></div>
                        <div class="second"></div>
                    </div>
                    <div class="right d-flex position-relative">
                        <img src="{{ asset('img/pic7.jpg') }}" class="" alt="..." loading="lazy">
                        <div class="block-txt position-absolute">
                            <p class="big-txt">
                                Follow your <br> dreams
                            </p>
                            <p class="small-txt">
                                Мы верим, что всё в жизни человека начинается с мечты. Любая достигнутая цель, любое
                                достижение - всё это когда-то было мечтой. Мы верим, что хорошее образование помогает
                                найти себя и реализовать свои мечты. Поэтому мы создали IBC Group - сообщество людей,
                                которые следуют за своей мечтой. Которые выбирают путь к мечте через зарубежное
                                образование.
                            </p>
                            <a class="watch-video video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/bfWKFGJdPR4" data-bs-target="#modalVideo">Смотреть видео <i class="bi bi-play-fill"></i></a>
                        </div>
                    </div>
                </div>
                <div class="bottom d-flex">
                    <div class="left">
                        <img src="{{ asset('img/pic8.jpg') }}" alt="" loading="lazy">
                    </div>
                    <div class="center">
                        <img src="{{ asset('img/pic4.jpg') }}" alt="" loading="lazy">
                    </div>
                    <div class="right">
                        <img src="{{ asset('img/pic5.jpg') }}" alt="" loading="lazy">
                    </div>
                </div>
            </div>
        </div>
        <div class="section-3 d-flex flex-column">
            <div class="top d-flex flex-lg-row flex-column">
                <div class="left d-flex flex-column">
                    <p class="big-txt">Work and Study USA</p>
                    <p class="small-txt">Мы прилагаем усилия, чтобы наши студенты получили образование в США. Мы собрали
                        весь наш опыт и
                        знания, и транслируем это на мастер-классах, на сайте и при сопровождении клиентов.</p>
                </div>
                <div class="right d-flex flex-wrap justify-content-center align-items-end">
                    <div class="d-flex element flex-column">
                        <p class="big-txt">#1</p>
                        <p class="small-txt">эксперты в РК <br>
                            по образованию <br>
                            в США</p>
                        <object data="/img/stars.svg" type=""></object>
                    </div>
                    <div class="d-flex element flex-column">
                        <p class="big-txt">10</p>
                        <p class="small-txt">филиалов <br>
                            по всему <br>
                            Казахстану</p>
                        <object data="/img/folders.svg" type=""></object>
                    </div>
                    <div class="d-flex element flex-column">
                        <p class="big-txt">98</p>
                        <p class="small-txt">проводим <br>
                            мастер-классов <br>
                            в год</p>
                        <object data="/img/messages.svg" type=""></object>
                    </div>
                    <div class="d-flex element flex-column">
                        <p class="big-txt">50</p>
                        <p class="small-txt">ТОП университетов <br>
                            сотрудничают с <br>
                            нами</p>
                        <object data="/img/globals.svg" type=""></object>
                    </div>
                    <div class="d-flex element flex-column">
                        <p class="big-txt">3 000</p>
                        <p class="small-txt">вакансий на период <br>
                            стажировки (OPT)</p>
                        <object data="/img/papers.svg" type=""></object>
                    </div>
                </div>
            </div>
            <div class="bottom d-flex">
                <div class="left d-flex align-items-end">
                    <div class="d-flex flex-column element" style="margin-right: 40px;">
                        <p class="big-txt">&gt;800</p>
                        <p class="small-txt d-lg-block">человек отправили <br>
                            в США на учебу</p>
                    </div>
                    <div class="d-flex flex-column element" style="margin-right: 40px;margin-bottom: 30px;">
                        <p class="big-txt">34 950</p>
                        <p class="small-txt">провели личных <br>
                            консультаций</p>
                    </div>
                    <div class="d-flex flex-column element" style="margin-bottom: 60px;">
                        <p class="big-txt">8 200</p>
                        <p class="small-txt">человек посетили <br> наши мастер- <br> классы</p>
                    </div>
                </div>
                <div class="right d-flex justify-content-end">
                    <div class="d-flex flex-column align-items-end box">
                        <p class="txt">— Приходите в наши офисы, посещайте мастер-классы, изучайте наш сайт - это всё
                            открыто для вас</p>
                        <a href="#" class="watch-video video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Uk7e06B4834" data-bs-target="#modalVideo">Смотреть видео <i class="bi bi-play-fill"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">


                    <div class="modal-body">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></span>
                        </button>
                        <!-- 16:9 aspect ratio -->
                        <div class="ratio ratio-16x9">
                            <iframe class="embed-responsive-item" src="" id="video" allowfullscreen="" allowscriptaccess="always" allow="autoplay" loading="lazy"></iframe>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="d-flex flex-lg-row flex-column justify-content-between end">
            <div class="d-flex flex-column"><p>IBC Group - команда экспертов в сфере
                    зарубежного образования. Наша миссия: предоставление доступного образования в США каждому желающему. Мы уже
                    отправили более 1000 студентов обучаться за рубежом.</p></div>
            <div class="d-flex flex-column"><p>На нашем сайте Вас ждут истории казахстанцев, которые следуя
                    за своей мечтой, выбирают путь получения зарубежного образования в США,
                    а также полезная информация о системе американского образования и жизни в США.</p></div>
        </div>
    </div>
    <div class="directions d-flex flex-lg-row flex-column">
        <div class="left">
            <p class="big-txt">Направления:</p>
            <p class="small-txt">
                В IBC Group более 13 направлений <br>
                в сфере образования и <br>
                трудоустройства за рубежом.
                <br>
                <br>
                Каждый год наши клиенты <br>
                открывают новые возможности, <br>
                меняют свою жизнь и достигают <br>
                больших результатов.
            </p>
            <a href="{{route('service.index', app()->getLocale())}}">Узнать подробнее</a>
        </div>
        <div class="right d-flex flex-lg-row flex-column justify-content-end">
			<a href="https://ibcg-usa.kz/" target="_blank" rel="noopener noreferrer">
				<div class="box1" style="background-image: url('/img/s1.jpg');"></div>
			</a>
			<a href="https://one-ibc.kz/" target="_blank" rel="noopener noreferrer">
				<div class="box2" style="background-image: url('/img/s2.jpg');"></div>
			</a>
			<a href="https://trip.usanaruss.com/" target="_blank" rel="noopener noreferrer">
				<div class="box3" style="background-image: url('/img/s3.jpg');"></div>
			</a>
        </div>
    </div>
@endsection

@section('form')
    <div class="form d-flex flex-lg-row flex-column">
        <div class="left d-flex justify-content-end">
            <div class="d-flex flex-column">
                <p class="big-txt">— Интересует <br>
                    сотрудничество?
                </p>
                <p class="small-txt">Компания открыта для <br>
                    всего нового и прогрессивного</p>
            </div>
        </div>
        <div class="right d-flex flex-column">
            <p class="big-txt">Оставить заявку</p>
            <p class="small-txt d-lg-block d-none">Внесите свои данные и наш специалист по развитию бизнеса <br>
                свяжется с вами.</p>
            <p class="small-txt d-lg-none d-block">Внесите свои данные и наш специалист по развитию бизнеса свяжется с
                вами.</p>
            <form method="post" action="{{ route('send-form', app()->getLocale()) }}">
                <div class="d-flex flex-column">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" name="name" aria-label="default" placeholder="Имя" required>
                    <input type="tel" name="phone" class="phone" aria-label="default" placeholder="Номер телефоная"
                           required>
                    <button type="submit" class="submit-button">Оставить заявку</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function() {

// Gets the video src from the data-src on each button

            var $videoSrc;
            $('.video-btn').click(function() {
                $videoSrc = $(this).data( "src" );
            });
            console.log($videoSrc);



// when the modal is opened autoplay it
            $('#modalVideo').on('shown.bs.modal', function (e) {

// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
                $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" );
            })



// stop playing the youtube video when I close the modal
            $('#modalVideo').on('hide.bs.modal', function (e) {
                // a poor man's stop video
                $("#video").attr('src',$videoSrc);
            })






// document ready
        });
    </script>
@endsection
