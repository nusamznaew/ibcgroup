<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
          integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="{{ asset('assets/css2/youCover.css') }}">
    <link rel="stylesheet" href="{{ mix('assets/css2/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css2/fonts.css') }}">
    <title>Откройте ваш бизнес в США</title>
</head>
<body>
<section id="header">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <a href="/" class="logo"><img src="{{ asset('img2/logo.png') }}" alt="logo"></a>
        </div>
    </div>
</section>
@yield('content')
<footer>
    <div class="container footer d-flex flex-lg-row flex-column justify-content-between">
        <p class="order-lg-1 order-3">&copy; USANARUSS.COM</p>
        <p class="order-lg-2 order-2">1920 E Hallandale Beach Blvd, USA</p>
        <a class="order-lg-3 order-1" href="tel:+1 (332) 209-93-38"> +1 (754) 581-73-52</a>
    </div>
</footer>
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
        integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src='{{ asset('assets/js/youCover.js') }}'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $(document).on('submit', 'form', function () {
            $('.submit-button').attr('disabled', 'disabled').css('cursor', 'not-allowed');
        });
    });
</script>
<script src="{{ asset('assets/js/test.js') }}"></script>
</body>
</html>
