<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="@yield('description', '')">
    <meta name="keywords" content="@yield('keywords', '')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--    <link rel="stylesheet" href="{{ mix('assets/css/bootstrap.css') }}">--}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Open Sans -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Title -->
    <title>@yield('title', '')</title>

    <link rel="stylesheet" href="{{ asset('assets/css/app.css?v1.2') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/algolia.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/slimselect.min.css') }}">
    @yield('extra-css')
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-219736416-1');
	</script>
</head>
<body>
<div class="container">
    <div class="d-flex justify-content-between main-block">
        @include('partials.navbar')
        <div class="content-block">
            @include('partials.header')
            @yield('content')
        </div>
    </div>
</div>
@yield('form')
@include('partials.footer')
@include('partials.modal')

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').removeClass('active');
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').addClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

<script src="{{ asset("/js/jquery.maskedinput.js") }}"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>
	
<script>
$(".phone").mask("+7(999)-999-99-99");

$(".phone").change(function () {
    if ($(".phone").val() === "+7(123)-456-78-90" ||
        $(".phone").val() === "+7(000)-000-00-00" ||
        $(".phone").val() === "+7(111)-111-11-11" ||
        $(".phone").val() === "+7(222)-222-22-22" ||
        $(".phone").val() === "+7(333)-333-33-33" ||
        $(".phone").val() === "+7(444)-444-44-44" ||
        $(".phone").val() === "+7(555)-555-55-55" ||
        $(".phone").val() === "+7(666)-666-66-66" ||
        $(".phone").val() === "+7(777)-777-77-77" ||
        $(".phone").val() === "+7(888)-888-88-88" ||
        $(".phone").val() === "+7(999)-999-99-99") {
        $(".phone").val("");
    }
});
</script>

@yield('extra-js')
</body>
</html>
