@extends('layouts.layout')

@section('title', 'Главная')

@section('description', 'Казахстанский портал о жизни в США')

@section('keywords', 'жизньвсша, нашивмире, нашивсша, казахивсша, жизньвамерике, новостиамерики, новостисша, интересноевсша, статьиобамерике, казахстанцывсша')

@section('extra-css')

@endsection

@section('content')
    <div class="dynamic-block">
        <div class="block-1 d-flex flex-lg-row flex-column justify-content-between">
            <div class="d-flex flex-column left justify-content-between">
                <div class="d-flex flex-column justify-content-between top">
                    <div id="carouselMainIndicators" class="carousel slide main-carousel" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            @foreach($banners as $banner)
                                @if($loop->index === 0)
                                    <button type="button" data-bs-target="#carouselMainIndicators"
                                            data-bs-slide-to="{{ $loop->index }}"
                                            class="active" aria-current="true"
                                            aria-label="Slide {{ $loop->index }}"></button>
                                @else
                                    <button type="button" data-bs-target="#carouselMainIndicators"
                                            data-bs-slide-to="{{ $loop->index }}"
                                            aria-label="Slide {{ $loop->index }}"></button>
                                @endif
                            @endforeach
                        </div>
                        <div class="carousel-inner">
                            @foreach($banners as $banner)
                                @if($loop->index === 0)
                                    <div class="carousel-item active">
                                        <a href="{{ $banner->link }}" target="_blank" rel="noopener noreferrer">
                                            <img src="{{ asset('storage/'. $banner->image) }}" class="d-block w-100"
                                                 alt="..." loading="lazy">
                                        </a>
                                    </div>
                                @else
                                    <div class="carousel-item">
                                        <a href="{{ $banner->link }}" target="_blank" rel="noopener noreferrer">
                                            <img src="{{ asset('storage/'. $banner->image) }}" class="d-block w-100"
                                                 alt="..." loading="lazy">
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselMainIndicators"
                                data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselMainIndicators"
                                data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
                <div class="d-flex flex-wrap bottom">
                    @foreach($instances2 as $instance)
                        <div class="d-flex flex-lg-column flex-row box">
                            <div class="d-flex">
                                <a href="{{ route('instances.show', [app()->getLocale() , $instance->slug]) }}" class="img">
                                    <div class="pic4a"
                                         style="background-image: url('{{ asset('storage/'. $instance->image) }}')"></div>
                                </a>
                            </div>
                            <div class="d-flex flex-column block-txt">
                                <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}"
                                   class="edu">{{ $instance->category }}</a>
                                <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}"
                                   class="big-text">{{ Str::limit($instance->title, 50) }}</a>
                                <div class="d-flex small-text">
                                    <p>{{ $instance->created_at->format('d M Y') }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="d-flex flex-column right">
                <a href="{{ route('masterclasses.index', app()->getLocale()) }}" class="head-link">{!! __('main.further_master_classes') !!}<i
                        class="bi bi-chevron-right"></i></a>
                <div class="d-flex flex-column justify-content-between afisha">
                    @foreach($masterclasses as $masterclass)
                        <a href="{{ route('masterclasses.show', [app()->getLocale(), $masterclass->slug]) }}">
                            <img src="storage/{{ $masterclass->image }}" alt="" loading="lazy">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="block-2 d-flex flex-lg-row flex-column justify-content-between">
            <div class="d-flex flex-column left">
                <div class="d-flex headline align-items-center">
                    <a href="{{ route('advices.index', app()->getLocale()) }}" class="me-auto">{!! __('main.useful') !!} <i class="bi bi-chevron-right"></i></a>
                    <div class="line"></div>
                </div>
                <div class="d-flex flex-wrap justify-content-between mini-banner">
                    @foreach($advices2 as $advice)
                        @if($loop->index === 0)
                            <div class="d-flex flex-column box position-relative"
                                 style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ asset('storage/'. $advice->image) }}');">
                                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="hidden-link"></a>
                                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="small-img">
                                    <div class="img"
                                         style="background-image: url('{{ asset('storage/'. $advice->image) }}');"></div>
                                </a>
                                <div class="d-flex flex-column toper">
                                    <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}"
                                       class="edu">{{ $advice->category }}</a>
                                    <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}"
                                       class="big-text">{{ Str::limit($advice->title, 50) }}</a>
                                    <div class="d-flex small-text">
                                        <p>{{ $advice->created_at->format('d M Y') }}</p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="d-flex flex-column box position-relative"
                                 style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ asset('storage/'. $advice->image) }}');">
                                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="hidden-link"></a>
                                <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}" class="small-img">
                                    <div class="img"
                                         style="background-image: url('{{ asset('storage/'. $advice->image) }}');"></div>
                                </a>
                                <div class="d-flex flex-column toper">
                                    <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}"
                                       class="edu">{{ $advice->category }}</a>
                                    <a href="{{ route('advices.show', [app()->getLocale(), $advice->slug]) }}"
                                       class="big-text">{{ Str::limit($advice->title, 50) }}</a>
                                    <div class="d-flex small-text">
                                        <p>{{ $advice->created_at->format('d M Y') }}</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="d-flex flex-column right">
                <div class="d-flex headline align-items-center">
                    <a href="{{ route('feedbacks.index', app()->getLocale()) }}" class="me-auto">{!! __('main.reviews') !!} <i class="bi bi-chevron-right"></i></a>
                    <div class="line"></div>
                </div>
                <div class="d-flex flex-column justify-content-between mini-banner h-100">
                    @foreach($feedbacks as $feedback)
                        <div class="d-flex flex-column box">
                            <a href="{{ route('feedbacks.show', [app()->getLocale(), $feedback->slug]) }}">
                                <div class="img"
                                     style="background-image: url('{{ asset('storage/'. $feedback->image) }}');"></div>
                            </a>
                            <a href="{{ route('feedbacks.show', [app()->getLocale(), $feedback->slug]) }}"
                               class="edu">{{ $feedback->name }}</a>
                            <a href="{{ route('feedbacks.show', [app()->getLocale(), $feedback->slug]) }}" class="description">
                                <div class="big-text">
                                    <p>{!! Str::limit($feedback->title, 120) !!}</p>
                                </div>
                            </a>
                            <div class="d-flex small-text">
                                <p>{{ $feedback->created_at->format('d M Y') }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="block-3">
            <div class="d-flex headline align-items-center">
                <a href="{{ route('service.index', app()->getLocale()) }}" class="me-auto">{!! __('main.our_services') !!} <i class="bi bi-chevron-right"></i></a>
                <div class="line"></div>
            </div>
            <div id="nashi-uslugi" class="d-flex flex-lg-row flex-column offers">
                @foreach($offers as $offer)
                    <a href="{{ $offer->link }}" target="_blank" rel="noopener noreferrer">
                        <div class="d-flex flex-column box"
                             style="background-image: url('storage/{{ $offer->image }}');">
                            <img src="storage/{{ $offer->image }}" loading="lazy" alt=""/>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="block-5">
            <div class="d-flex headline align-items-center">
                <a href="{{ route('instances.index', app()->getLocale()) }}" class="me-auto">{!! __('main.cases') !!} <i class="bi bi-chevron-right"></i></a>
                <div class="line"></div>
            </div>

            <div class="d-flex flex-wrap">
                @foreach($instances as $instance)
                    <div class="d-flex flex-lg-column flex-row flag">
                        <div>
                            <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}">
                                <div class="d-flex flex-column box"
                                     style="background-image: url('{{ asset('storage/'. $instance->image) }}')"></div>
                            </a>
                        </div>
                        <div class="d-flex flex-column block-txt">
                            <a href="{{ route('instances.show', [app()->getLocale(), $instance->slug]) }}"
                               class="card-title">{{ Str::limit($instance->title, 50) }}</a>
                            <p class="card-subtitle">{{ $instance->created_at->format('d M Y') }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

@endsection
