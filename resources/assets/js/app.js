require('./bootstrap');

import SlimSelect from "slim-select";

new SlimSelect({
    select: '#industry',
    placeholder: 'Индустрии',
    searchText: 'Извините, мы не смогли ничего найти...'
})

new SlimSelect({
    select: '#city',
    placeholder: 'Город',
    searchText: 'Извините, мы не смогли ничего найти...'
})

$(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});

$(".phone").mask("+7(999)-999-99-99");

$(".phone").change(function () {
    if ($(".phone").val() === "+7(123)-456-78-90" ||
        $(".phone").val() === "+7(000)-000-00-00" ||
        $(".phone").val() === "+7(111)-111-11-11" ||
        $(".phone").val() === "+7(222)-222-22-22" ||
        $(".phone").val() === "+7(333)-333-33-33" ||
        $(".phone").val() === "+7(444)-444-44-44" ||
        $(".phone").val() === "+7(555)-555-55-55" ||
        $(".phone").val() === "+7(666)-666-66-66" ||
        $(".phone").val() === "+7(777)-777-77-77" ||
        $(".phone").val() === "+7(888)-888-88-88" ||
        $(".phone").val() === "+7(999)-999-99-99") {
        $(".phone").val("");
    }
});

function validate(input) {
    if (/^\s/.test(input.value))
        input.value = '';
}


$(document).ready(function () {
    $(document).on('submit', 'form', function () {
        $('.submit-button').attr('disabled', 'disabled').css('cursor', 'not-allowed');
    });
});
