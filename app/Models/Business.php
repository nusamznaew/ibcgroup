<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function city()
    {
        return $this->hasOne(City::class,'id','city_id');
    }

    public function industry()
    {
        return $this->hasOne(City::class,'id','industry_id');
    }
}
