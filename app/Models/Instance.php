<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Instance extends Model implements Searchable
{
    use HasFactory;

    protected $fillable = [
        'title'
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('instances.show', [app()->getLocale(), $this->slug]);

        return new SearchResult(
            $this,
            $this->title,
            $url
        );
    }
}
