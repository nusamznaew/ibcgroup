<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMasterclass extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return SendMasterclass
     */
    public function build()
    {

        $name = $this->data['name'];
        $email = $this->data['email'];
        $phone = $this->data['phone'];
        $masterclass = $this->data['masterclass'];


        return $this->subject('Заявка на мастер класс')
            ->view('mail.masterclass-mail', compact('name', 'email', 'phone', 'masterclass'));
    }
}
