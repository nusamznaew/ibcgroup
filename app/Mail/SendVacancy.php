<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Console\Input\Input;

class SendVacancy extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return SendVacancy
     */
    public function build()
    {

        $name = $this->data['name'];
        $position = $this->data['position'];
        $phone = $this->data['phone'];
        $social = $this->data['social'];
        $description = $this->data['description'];

        if ($this->data['file'] != null) {
            return $this->subject('Отклик на вакансию')
                ->view('mail.mail', compact('name', 'position', 'phone', 'social', 'description'))
                ->attach($this->data['file']->getRealPath(), [
                    'as' => $this->data['file']->getClientOriginalName()
                ]);
        }

        return $this->subject('Отклик на вакансию')
            ->view('mail.mail', compact('name', 'position', 'phone', 'social', 'description'));
    }
}
