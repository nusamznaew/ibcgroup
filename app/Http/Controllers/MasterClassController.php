<?php

namespace App\Http\Controllers;

use App\Mail\SendMasterclass;
use App\Models\MasterClass;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class MasterClassController extends Controller
{

    public function index()
    {
        $masterclasses = MasterClass::orderBy('order', 'asc')->where('featured', true)->get();
        Carbon::setLocale('ru');
        return view('pages.masterclasses')->with('masterclasses', $masterclasses);
    }

    public function show($slug)
    {
        $masterclass = MasterClass::where('slug', $slug)->firstOrFail();
        $similarMasterclasses = MasterClass::orderBy('order', 'asc')->where('slug', '!=', $slug)->where('featured', true)->get();

        return view('pages.masterclass')->with([
            'masterclass' => $masterclass,
            'similarMasterclasses' => $similarMasterclasses
        ]);
    }

    public function send(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'masterclass' => $request->masterclass
        ];
        Mail::to('ibcportal@yandex.kz')->send(new SendMasterclass($data));
        return redirect()->route('success');
    }
}
