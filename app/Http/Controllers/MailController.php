<?php

namespace App\Http\Controllers;

use App\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send(Request $request)
    {
        $data = [
            'name' => $request->name,
            'city' => $request->city,
            'phone' => $request->phone,
			'point' => $request->point
        ];

        Mail::to('form.ibcg@yandex.kz')->send(new Mailer($data));
        return redirect()->route('success', app()->getLocale());
    }
}
