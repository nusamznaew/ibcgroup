<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\Headline;

class FeedbackController extends Controller
{
    public function index()
    {
        $feedbacks = Feedback::orderBy('created_at', 'desc')->where('featured', true)->get();

        return view('pages.feedbacks')->with('feedbacks', $feedbacks);
    }

    public function show($slug)
    {
        $feedback = Feedback::where('slug', $slug)->where('featured', true)->firstOrFail();
        $similarFeedbacks = Feedback::where('slug', '!=', $slug)->where('featured', true)->inRandomOrder()->get();

        return view('pages.feedback')->with([
            'feedback' => $feedback,
            'similarFeedbacks' => $similarFeedbacks
        ]);
    }
}
