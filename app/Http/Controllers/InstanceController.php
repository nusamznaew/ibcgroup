<?php

namespace App\Http\Controllers;

use App\Models\Headline;
use App\Models\Instance;
use Illuminate\Http\Request;
use Log;

class InstanceController extends Controller
{
    public function index()
    {
        $instances = Instance::orderBy('created_at', 'desc')->where('featured', true)->get();

        return view('pages.instances')->with('instances', $instances);
    }

    public function show($slug)
    {
        $instance = Instance::where('slug', '=', $slug)->firstOrFail();
        $similarInstances = Instance::where('slug', '!=', $slug)->where('featured', true)->inRandomOrder()->get();

        return view('pages.instance')->with([
            'instance' => $instance,
            'similarInstances' => $similarInstances
        ]);
    }
}
