<?php

namespace App\Http\Controllers;

use App\Models\Advice;
use App\Models\Banner;
use App\Models\Feedback;
use App\Models\Headline;
use App\Models\Instance;
use App\Models\MasterClass;
use App\Models\Offer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Nette\Utils\DateTime;
use Spatie\Searchable\Search;

class MainPageController extends Controller
{
    public function index()
    {
        $instances2 = Instance::orderBy('created_at', 'desc')->where('featured', true)->take(3)->get();
        $advice2 = Advice::inRandomOrder()->where('featured', true)->take(4)->get();
        $instances = Instance::inRandomOrder()->where('featured', true)->take(8)->get();
        $feedback = Feedback::inRandomOrder()->where('featured', true)->take(2)->get();
        $masterclasses = MasterClass::orderBy('order', 'asc')->where('featured', true)->take(2)->get();
        $offers = Offer::orderBy('created_at', 'asc')->take(4)->get();
        $banners = Banner::orderBy('order', 'asc')->get();

        return view('main')->with([
            'instances2' => $instances2,
            'advices2' => $advice2,
            'instances' => $instances,
            'feedbacks' => $feedback,
            'banners' => $banners,
            'offers' => $offers,
            'masterclasses' => $masterclasses
        ]);
    }

    public function search(Request $request)
    {
        if ($request->input('query') === null) {
            return redirect(route('main'));
        }

        $searchResults = (new Search())
            ->registerModel(Headline::class, 'title')
            ->registerModel(Advice::class, 'title')
            ->registerModel(MasterClass::class, 'title')
            ->registerModel(Instance::class, 'title')
            ->registerModel(Feedback::class, 'title')
            ->perform($request->input('query'));

        return view('pages.search', compact('searchResults'));

    }
}
