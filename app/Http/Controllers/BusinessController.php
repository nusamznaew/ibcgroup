<?php

namespace App\Http\Controllers;

use App\Mail\SendBusiness;
use App\Models\Business;
use App\Models\City;
use App\Models\Industry;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BusinessController extends Controller
{
    public function index(Request $request)
    {

        $businessesQuery = Business::query();

        if ($request->filled('price_from')) {
            $businessesQuery->where('asking_price', '>=', $request->price_from);
        }

        if ($request->filled('price_to')) {
            $businessesQuery->where('asking_price', '<=', $request->price_to);
        }

        if ($request->has('fb')) {
            $businessesQuery->whereIn('fb', $request->input('fb'));
        }

        if ($request->has('city')) {
            $businessesQuery->whereIn('city_id', $request->input('city'));
        }

        if ($request->has('industry')) {
            $businessesQuery->whereIn('industry_id', $request->input('industry'));
        }

//        dd($request->all());

        $businesses = $businessesQuery->orderBy('created_at', 'desc')->where('featured', true)->paginate(10);

//        dd($businesses);

        $cities = City::orderBy('name')->get();

        $industries = Industry::orderBy('name')->get();

        return view('pages.businesses')->with([
            'businesses' => $businesses,
            'cities' => $cities,
            'industries' => $industries
        ]);
    }

    public function show($slug)
    {
        $business = Business::where('slug', $slug)->firstOrFail();
        $similarBusinesses = Business::orderBy('created_at', 'desc')->where('slug', '!=', $slug)->where('featured', true)->take(10)->get();


        return view('pages.business')->with([
            'business' => $business,
            'similarBusinesses' => $similarBusinesses
        ]);
    }

    public function send(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'business' => $request->business
        ];
        Mail::to('business.buy@yandex.kz')->send(new SendBusiness($data));
        return redirect()->route('success');
    }
}
