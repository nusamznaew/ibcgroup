<?php

namespace App\Http\Controllers;

use App\Models\Advice;
use App\Models\Headline;
use Illuminate\Http\Request;
use ProtoneMedia\LaravelCrossEloquentSearch\Search;

class SearchController extends Controller
{
    public function __invoke(Request $request)
    {

        $results = Search::addMany([
            [Headline::class, 'title'],
            [Advice::class, 'title'],
        ])
            ->beginWithWildcard()
            ->paginate()
            ->get($request->get('key'));

        return view('pages.search', compact('results'));
    }
}
