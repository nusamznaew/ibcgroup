<?php

namespace App\Http\Controllers;

use App\Models\MassMedia;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index()
    {
        $medias = MassMedia::orderBy('created_at', 'desc')->where('featured', true)->get();

        return view('pages.mass-media')->with('medias', $medias);
    }
}
