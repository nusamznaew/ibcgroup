<?php

namespace App\Http\Controllers;

use App\Models\Advice;
use App\Models\Headline;

class AdviceController extends Controller
{

    public function index()
    {
        $advices = Advice::orderBy('created_at', 'desc')->where('featured', true)->get();

        return view('pages.advices')->with('advices', $advices);
    }

    public function show($slug)
    {
        $advice = Advice::where('slug', $slug)->where('featured', true)->firstOrFail();
        $similarAdvices = Advice::where('slug', '!=', $slug)->where('featured', true)->inRandomOrder()->get();

        return view('pages.advice')->with([
            'advice' => $advice,
            'similarAdvices' => $similarAdvices
        ]);
    }
}
