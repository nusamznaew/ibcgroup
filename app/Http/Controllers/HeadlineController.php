<?php

namespace App\Http\Controllers;

use App\Models\Headline;
use Illuminate\Http\Request;

class HeadlineController extends Controller
{
    public function index()
    {
        $headlines = Headline::inRandomOrder()->where('featured', true)->take(1)->get();

        foreach ($headlines as $headline) {
            $similarHeadlines = Headline::where('slug', '!=', $headline->slug,)->where('featured', true)->inRandomOrder()->get();
        }

        return view('pages.headlines')->with([
            'headlines' => $headlines,
            'similarHeadlines' => $similarHeadlines
        ]);
    }

    public function show($slug)
    {
        $headline = Headline::where('slug', $slug)->where('featured', true)->firstOrFail();
        $similarHeadlines = Headline::where('slug', '!=', $slug)->where('featured', true)->inRandomOrder()->get();

        return view('pages.headline')->with([
            'headline' => $headline,
            'similarHeadlines' => $similarHeadlines
        ]);
    }
}
