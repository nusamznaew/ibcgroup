<?php

namespace App\Http\Controllers;

use App\Mail\SendVacancy;
use App\Models\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class VacancyController extends Controller
{
    public function index()
    {
        $vacancies = Vacancy::orderBy('created_at', 'desc')->get();

        return view('pages.vacancy')->with('vacancies', $vacancies);
    }

    public function send(Request $request)
    {

        $data = [
            'name' => $request->name,
            'position' => $request->position,
            'phone' => $request->phone,
            'social' => $request->social,
            'description' => $request->description,
            'file' => $request->file('file')
        ];

        $to = 'job@ibcg.kz';

        Mail::to($to)->send(new SendVacancy($data));

        return redirect()->route('success');
    }
}
